$(document).ready(function () {
    var docTitle = $('meta[name="pageName"]').attr('content');

/////////////////////////   Button to top   /////////////////////////

    var oldScrollPosition = $(window).scrollTop(),
        savedScrollPosition;

    $("#id_to_top").on("click", "a", function (event) {
        event.preventDefault();
        savedScrollPosition = $(window).scrollTop();
        $('body,html').animate({scrollTop: 0}, 1500);
        $("#id_to_previous_scroll").addClass("custom_show");
    });

    $("#id_to_previous_scroll").on("click", "a", function (event) {
        $('body,html').animate({scrollTop: savedScrollPosition}, 1500);
        $("#id_to_previous_scroll").removeClass("custom_show");
    });

    $(document).on("scroll", "", function () {
        var newScrollPosition = $(window).scrollTop();
        if (newScrollPosition > oldScrollPosition) {
            $("#id_to_previous_scroll").removeClass("custom_show");
        }
        oldScrollPosition = newScrollPosition;
    });

////////////////////////___Button to top___////////////////////////

/////   Navbar on scroll, Modal_viewpicture body height on resize   /////

    // Navbari chisht scroll-i chap@ poxel 480-ic cacr width unecox deviceneri vra
    var w = window.innerWidth;
    if (w > 991) {
        $('#myNavbar').attr('data-offset-top', '121');
    } else if (w <= 991 && w > 480) {
        $('#myNavbar').attr('data-offset-top', '160');
    } else if (w <= 480 && w > 393) {
        $('#myNavbar').attr('data-offset-top', '193');
    } else {
        $('#myNavbar').attr('data-offset-top', '212');
    }

    // resize aneluc dynamic poxel
    $(window).resize(function () {
        var w = window.innerWidth;
        if (w > 991) {
            $('#myNavbar').attr('data-offset-top', '121');
        } else if (w <= 991 && w > 480) {
            $('#myNavbar').attr('data-offset-top', '160');
        } else if (w <= 480 && w > 393) {
            $('#myNavbar').attr('data-offset-top', '193');
        } else {
            $('#myNavbar').attr('data-offset-top', '212');
        }

        // modal viewpicture-i body-i height@ poxel resize aneluc (product_details.html)
        var modalBodyHeight = document.documentElement.clientHeight - $("#id_modal_viewpicture_header").outerHeight()
            - $("#id_modal_viewpicture_footer").outerHeight() - 40;
        $("#id_modal_viewpicture_body").height(modalBodyHeight);
    });

////___Navbar on scroll, Modal_viewpicture body height on resize___////

///////////////////   Login and Register modal   ///////////////////
    $("#login_button").on('click', function (e) {
        e.preventDefault();
        loginRegisterModal('login');
    });
    $("#register_button").on('click', function (e) {
        e.preventDefault();
        loginRegisterModal('register');
    });

    // If clicked on login, open modal "login" tab opened, else
    // if clicked on register, open modal "register" tab opened

    var hashAddress = document.location.hash;

    if (hashAddress === "#login" && $("#login_button" + name).length !== 0) {
        // Ete #login e  url-um, apa stugi, ete user@ login exac chi, apa baci ayd modal@
        $('#id_modalloginregister').modal({});
    } else if (hashAddress === "#register" && $("#register_button" + name).length !== 0) {
        // Ete #register e  url-um, apa stugi, ete user@ login exac chi, apa baci ayd modal@
        $('#id_modalloginregister').modal({});
        $("#id_login_header").removeClass("active");
        $("#id_register_header").addClass("active");

        $("#id_login_tab").removeClass("active in");
        $("#id_register_tab").addClass("active in");
    } else if (hashAddress === "#reset" && $("#register_button" + name).length !== 0) {
        // Ete #login kam register e  url-um, apa stugi, ete user@ login exac chi, apa baci ayd modal@
        $('#id_modalpasswordreset').modal({});
        // Ete #resettoken e apa baci ayd modal@
    } else if (hashAddress.substring(0, 11) === "#resettoken") {
        $('#id_modalpasswordreset').modal({});
        $("#id_reset_button").on('click', function (e) {
            e.preventDefault();
            var emailField = $('#id_reset_email'),
                emailErrorField = $('#id_reset_email_error'),
                email = $(emailField).val(),
                passwordField = $('#id_reset_password'),
                passwordErrorField = $('#id_reset_password_error'),
                passwordConfirmationField = $('#id_reset_password_confirmation'),
                passwordConfirmationErrorField = $('#id_reset_password_confirmation_error'),
                password = $(passwordField).val();

            if (inputFieldValidation('email', emailField, emailErrorField) &&
                inputPasswordValidation(passwordField, passwordErrorField, passwordConfirmationField, passwordConfirmationErrorField)) {
                ajaxResetPassword(email, password);
            }
        })
    }

    // Change tabs (in modal)
    $('#id_login_register_menu').on("click", "a", function (e) {
        e.preventDefault();
        $(this).tab('show')
    });

//////////////////___Login and Register modal___//////////////////

//////////////////////////////   Login   /////////////////////////////

    $('#id_signin').on("click", function () {
        var emailField = $('#id_login_email'),
            emailErrorField = $('#id_loginerror'),
            email = $(emailField).val(),
            password = $('#id_login_password').val(),
            keep_signed_in = $('#id_keep_signed_in').is(':checked');

        if (inputFieldValidation('email', emailField, emailErrorField)) {
            ajaxLogin(email, password, keep_signed_in);
        }
    });

////////////////////////////___Login___////////////////////////////

////////////////////////////   Register   ////////////////////////////

    var validationObject = {
        'name': false,
        'surname': false,
        'email': false,
        'password': false
    };

    $('#id_register_form').on('blur', 'input', function () {
        validationObject = registerValidation(this, validationObject);
    });

    $('#id_sign_up_button').on('click', function (e) {
        e.preventDefault();
        var validation = true;

        $.each(validationObject, function (index, value) {
            if (value === false) {
                registerValidation($('#id_register_' + index), validationObject);
                validation = false;
            }
        });
        if (validation === true) {
            var name = $("#id_register_name").val(),
                surname = $("#id_register_surname").val(),
                email = $("#id_register_email").val(),
                password = $("#id_register_password").val(),
                password_confirmation = $("#id_register_password_confirmation").val();

            ajaxRegister(name, surname, email, password, password_confirmation);
        }
    });

    $('#id_register_email').on('blur', function () {
        if (validationObject['email'] === true) {
            ajaxEmailCheck($(this).val(), $('#' + $(this).attr('id') + '_error'))
        }
    });

///////////////////////////___Register___///////////////////////////

////////////////////   Send reset password link   ///////////////////

    $("#id_reset_password_link").on('click', function (e) {
        e.preventDefault();
        $("#id_login_tab").replaceWith(
            '<div id="id_login_tab" class="text-center tab-pane fade register_tab active in">\n' +
            '<div class="loginerror errorhide" id="id_send_reset_email_error">\n' +
            '</div>\n' +
            '<div class="form-group">\n' +
            '<input type="text" class="form-control input-lg" id="id_send_reset_email"\n' +
            'placeholder="Email address" name="email">\n' +
            '</div>\n' +
            ' <button type="button" class="btn  center-block login_btn" id="id_password_reset_link_button">' +
            'Send Password Reset Link' +
            '</button>\n' +
            ' </div>\n'
        );

        $("#id_password_reset_link_button").on('click', function () {
            var field = $('#id_send_reset_email'),
                errorField = $('#id_send_reset_email_error'),
                email = $(field).val();

            if (inputFieldValidation('email', field, errorField)) {
                ajaxSendResetEmail(email, errorField);
            }
        })
    });

//////////////////___Send reset password link___//////////////////


//////////////////////  Bootstrap carousels   //////////////////////

    if (docTitle === "Home") {
        // Carousels
        $('#id_carousel1').bsTouchSlider();
    }

    if (docTitle === "Home" || docTitle === "Product details") {
        // Carousels
        $('#id_carousel2').bsTouchSlider();
    }

/////////////////////___Bootstrap carousels___/////////////////////

///////////////////////   Products (by ajax)   ///////////////////////

    if (docTitle === "Home" || docTitle === "Category" || docTitle === "Categorygroup") {
        var productsField = $("#productsField"),
            productsUrl = $('meta[name="route_front.products"]').attr('content'),
            url = document.location.hash.replace('#', '?'),
            queryObject = urlToArray(url),
            filterForm = $('#filterForm'),
            filterFormInputs = $(filterForm).find('input'),
            priceRange = $("#priceRange");

        fillInputsFromObject(filterFormInputs, queryObject);
        $(priceRange).slider({});


        if (docTitle === "Category") {
            queryObject['categoryId'] = $('meta[name="categoryId"]').attr('content');
        } else if (docTitle === "Categorygroup") {
            queryObject['categorygroupId'] = $('meta[name="categorygroupId"]').attr('content');
        }

        ajaxGetProductsPage(productsField, productsUrl, queryObject);

        $(productsField).on('click', '.pagination a', function (e) {
            e.preventDefault();
            var pageValue = $(this).html();

            switch (pageValue) {
                case '«':
                    queryObject['page'] = queryObject['page'] - 1;
                    break;
                case '»':
                    queryObject['page'] = +queryObject['page'] + 1;
                    break;
                default:
                    queryObject['page'] = pageValue;
            }

            updateHashAddress(queryObject);
            ajaxGetProductsPage(productsField, productsUrl, queryObject);
        });

        $(productsField).on('input', '*[data-filter]', function () {
            var parameter = $(this).attr('data-filter');
            queryObject[parameter] = $(this).val();
            queryObject['page'] = 1;

            updateHashAddress(queryObject);
            ajaxGetProductsPage(productsField, productsUrl, queryObject);
        });

        $(productsField).on('click', '*[data-order]', function (e) {
            e.preventDefault();
            queryObject['order'] = $(this).attr('data-order');

            updateHashAddress(queryObject);

            ajaxGetProductsPage(productsField, productsUrl, queryObject);
        });

        ////////////////////////////  HACK  ////////////////////////////
        // Mouseup not working, when mouse released not in same element //
        //              It only works correctly, when relased on document               //

        var mousePressedOnRange = false;
        $(priceRange).parent().children(":first").on('mousedown', function () {
            console.log('mousedown');
            mousePressedOnRange = true;
        });

        $(document).on('mouseup', function () {
            console.log('mouseup');
            if (mousePressedOnRange === true) {
                var formObject = urlToArray($(filterForm).serialize());

                removeFromObjectByKeyPrefix(queryObject, 'brand');
                removeFromObjectByKeyPrefix(queryObject, 'parameter#');
                queryObject = Object.assign(queryObject, formObject);
                queryObject['page'] = 1;
                updateHashAddress(queryObject);
                ajaxGetProductsPage(productsField, productsUrl, queryObject);
                mousePressedOnRange = false;
            }
        });

        ///////////////////////////__HACK__///////////////////////////

        $(filterFormInputs).on('input', function (e) {
            e.preventDefault();
            var formObject = urlToArray($(filterForm).serialize());

            removeFromObjectByKeyPrefix(queryObject, 'availibility');
            removeFromObjectByKeyPrefix(queryObject, 'brand');
            removeFromObjectByKeyPrefix(queryObject, 'parameter#');
            queryObject = Object.assign(queryObject, formObject);
            queryObject['page'] = 1;
            updateHashAddress(queryObject);
            ajaxGetProductsPage(productsField, productsUrl, queryObject);
        });


    }

//////////////////////___Products (by ajax)___//////////////////////

////////////////////////////   Product   ////////////////////////////

    if (docTitle === "Product") {

        ////   On Quantity change   ////

        var productDetailQuantity = $('#id_product_details_quantity');

        // Input aneluc toxel miayn tver@
        $(productDetailQuantity).on("input", "", function () {
            if ($(this).val() !== "") {
                $(this).val($(this).val().replace(/[^\d]/g, ""));
                if ($(this).val() === "0") {
                    $(this).val("");
                } else if (+$(this).val() > 99) {
                    do {
                        $(this).val($(this).val().substring(0, $(this).val().length - 1));
                    } while (+$(this).val() > 99);
                }
            }
        });

        // Inputic heracneluc ete datark e, sarqi 1
        $(productDetailQuantity).on("blur", "", function () {
            if ($(this).val() === "") {
                $(this).val("1");
            }
        });

        ////___On Quantity change___////

        ////   Zoom on picture hover   ////

        // disable on mobile devices
        var currentImage = $("#id_viewcurrentproduct img");
        if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
            $(currentImage).ezPlus({
                zIndex: 2,
                cursor: 'pointer'
            });
        }

        ////___Zoom on picture hover___////

        ////   Owl carousel   ////

        var owlCarouselProductImages = $("#id_owl_carousel_product_images"),
            currentImageSrc = currentImage.attr('src'),
            allImagesInCarousel = $($(owlCarouselProductImages).html()).clone(),
            items_In_page_carousel = $(owlCarouselProductImages).find(".item"),
            items_In_modal_carousel = [];

        // Owlcarousel on page
        owlCarouselProductImages.owlCarousel({
            loop: false,
            dots: false,
            nav: true,
            mouseDrag: false,
            slideBy: 2,
            navContainer: '#id_owl_nav_controls',
            navElement: 'a',
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 3
                },
                320: {
                    items: 3
                },
                480: {
                    items: 4
                },
                560: {
                    items: 5
                },
                768: {
                    items: 3
                }
            }
        });

        // owl carouseli arajin elementin talis e sev border

        $(items_In_page_carousel[0]).addClass(" border_black");
        var selectedImage = $(".border_black");
        var selectedImageInmodal, currentImageSrcInModal;

        //  mouseover & select  //

        $(owlCarouselProductImages).on("mouseover", ".item", function () {
            currentImage.attr("src", $(this.firstChild).attr('src'));
            $(this.firstChild).attr('src');
        });

        $(owlCarouselProductImages).on("click", ".item", function () {
            $(selectedImage).removeClass("border_black");
            selectedImage = $(this);
            $(selectedImage).addClass("border_black");
            currentImageSrc = currentImage.attr('src');
            if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
                $(currentImage).ezPlus({
                    zIndex: 2,
                    cursor: 'pointer'
                });
            }
        });

        $(owlCarouselProductImages).on("mouseout", ".item", function () {
            currentImage.attr("src", currentImageSrc);
        });

        //__mouseover & select__//

        ///  image view modal  ///

        var modalOneTimeOpened = false,
            owlCarouselInModal = $('#id_owl_carousel_in_modal'),
            modalViewpciture = $("#id_modalviewpicture");

        // Currentimage-i vra click aneluc
        $(currentImage).on("click", "", function () {
            $(modalViewpciture).modal({});
            $("#id_modaltitle").text($("#id_productname").text());
            $('#id_modalviewpicture_currentimage').attr("src", currentImageSrc);

            // ete voch mi angam chi bacvel, stexcel owlcarousel
            if (modalOneTimeOpened === false) {
                modalOneTimeOpened = true;
                owlCarouselInModal.html(allImagesInCarousel);
                owlCarouselInModal.owlCarousel({
                    loop: false,
                    dots: false,
                    nav: true,
                    mouseDrag: false,
                    slideBy: 2,
                    navContainer: '#id_owl_in_modal_nav_controls',
                    navElement: 'a',
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    margin: 20,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 3
                        },
                        320: {
                            items: 3
                        },
                        480: {
                            items: 4
                        },
                        560: {
                            items: 5
                        },
                        768: {
                            items: 10
                        }
                    }
                });
            }

            items_In_modal_carousel = $("#id_owl_carousel_in_modal .item");

            // ayn owl-i element@, vor@ ejum sev borderov e, modali owl-i hamapatasxan elementi vra arhestakan
            // click anel, vorpeszi nuynpes darna sev borderov ev modali ej@ tertel ayd elementi vra
            var tempindex1 = getElementIndexByClassname(items_In_page_carousel, "border_black");
            $(items_In_modal_carousel[tempindex1]).trigger("click");
            $(owlCarouselInModal).trigger("to.owl.carousel", [tempindex1 - 1, 1, true]);

            // hide current image nav controls in modal
            if (items_In_modal_carousel.length === 1) {
                $(modalViewpictureNavLeft).addClass("hide");
                $(modalViewpictureNavRight).addClass("hide")
            } else {
                var tempIndex2 = getElementIndexByClassname(items_In_modal_carousel, "border_black");
                if (tempIndex2 <= 0) {
                    $(modalViewpictureNavLeft).addClass("off");
                    $(modalViewpictureNavRight).removeClass("off");
                } else if (tempIndex2 >= items_In_modal_carousel.length - 1) {
                    $(modalViewpictureNavRight).addClass("off");
                    $(modalViewpictureNavLeft).removeClass("off");
                } else {
                    $(modalViewpictureNavLeft).removeClass("off");
                    $(modalViewpictureNavRight).removeClass("off");
                }
            }
        });

        // owlcarousel@ refresh lineluc modali body-i height@ poxel
        $(owlCarouselInModal).on('refreshed.owl.carousel', function () {
            var modalBodyHeight = document.documentElement.clientHeight - $("#id_modal_viewpicture_header").outerHeight()
                - $("#id_modal_viewpicture_footer").outerHeight() - 40;
            $("#id_modal_viewpicture_body").height(modalBodyHeight);
        });

        //  mouseover & select in modal //

        var currentImageInModal = $("#id_modalviewpicture_currentimage");

        $(owlCarouselInModal).on("mouseover", ".item", function () {
            currentImageInModal.attr("src", $(this.firstChild).attr('src'));
            $(this.firstChild).attr('src');
        });

        $(owlCarouselInModal).on("click", ".item", function () {
            $(selectedImageInmodal).removeClass("border_black");
            selectedImageInmodal = $(this);
            $(selectedImageInmodal).addClass("border_black");
            currentImageSrcInModal = $(this.firstChild).attr("src");
            currentImageInModal.attr("src", currentImageSrcInModal);

            // poxel slaqneri vichak@, ete petq e
            var currentIndex = getElementIndexByClassname(items_In_modal_carousel, "border_black");
            if (currentIndex <= 0) {
                $(modalViewpictureNavLeft).addClass("off");
                $(modalViewpictureNavRight).removeClass("off");
            } else if (currentIndex >= items_In_modal_carousel.length - 1) {
                $(modalViewpictureNavRight).addClass("off");
                $(modalViewpictureNavLeft).removeClass("off");
            } else {
                $(modalViewpictureNavLeft).removeClass("off");
                $(modalViewpictureNavRight).removeClass("off");
            }
        });

        $(owlCarouselInModal).on("mouseout", ".item", function () {
            currentImageInModal.attr("src", currentImageSrcInModal);
        });

        //__mouseover & select in modal__//

        //  current image nav controls in modal  //

        var modalViewpictureNavLeft = $("#id_modal_viewpicture_nav_left"),
            modalViewpictureNavRight = $("#id_modal_viewpicture_nav_right");

        // dzax buttonin sexmeluc
        $(modalViewpictureNavLeft).on("click", "", function () {
            var currentIndex = getElementIndexByClassname(items_In_modal_carousel, "border_black");
            $(items_In_modal_carousel[currentIndex - 1]).trigger("click");
            $(owlCarouselInModal).trigger("to.owl.carousel", [currentIndex - 2, 700, true]);

            $(modalViewpictureNavRight).removeClass("off");
            if (currentIndex <= 1) {
                $(modalViewpictureNavLeft).addClass("off");
            } else {
                $(modalViewpictureNavLeft).removeClass("off");
            }
        });

        // aj buttonin sexmeluc
        $(modalViewpictureNavRight).on("click", "", function () {
            var currentIndex = getElementIndexByClassname(items_In_modal_carousel, "border_black");
            $(items_In_modal_carousel[currentIndex + 1]).trigger("click");
            $(owlCarouselInModal).trigger("to.owl.carousel", [currentIndex, 700, true]);

            $(modalViewpictureNavLeft).removeClass("off");
            if (currentIndex > items_In_modal_carousel.length - 3) {
                $(modalViewpictureNavRight).addClass("off");
            } else {
                $(modalViewpictureNavRight).removeClass("off");
            }
        });

        // keyboard-i aj u dzax arrowner@ sexmeluc
        $(document).on("keydown", "", function (e) {
            if ($(modalViewpciture[0]).hasClass("in") === true) {
                switch (e.originalEvent.key) {
                    case "ArrowLeft":
                        modalViewpictureNavLeft.trigger("click");
                        break;
                    case "ArrowRight":
                        modalViewpictureNavRight.trigger("click");
                        break;
                }
            }
        });

        // touch-i aj u dzax aneluc
        new Swipe(document.getElementById("id_modal_viewpicture_body"), function (event, direction) {
            event.preventDefault();
            switch (direction) {
                case "left":
                    modalViewpictureNavRight.trigger("click");
                    break;
                case "right":
                    modalViewpictureNavLeft.trigger("click");
                    break;
            }
        });

        //__current image nav controls in modal__//

        ///__image view modal__///
    }

    if (docTitle === "Product") {
        $("#addToCart").on('click', function () {
            if ($(this).attr('data-auth') === 'guest') {
                loginRegisterModal('login', 'Please login first');
            } else {
                var quantity = $(this).prev().val();
                ajaxAddToCart($(this).attr('data-productId'), quantity);
            }
        })
    }


///////////////////////////___Product___///////////////////////////

//////////   Paginations   //////////

// if (docTitle === "Products") {
//     // Paginations in "products.html"
//     // pagenumber_right elementi naxord "a"-i id-ic vercnenq verjin eji hamar@
//     var maxPageNumber = +$("#id_pagenumber_right").parent().prev().children(":first").attr('id').replace(/^\D+/g, ''),
//         currentPageNumber = 1;
//     $('#id_pagination').on("click", "a", function (e) {
//         e.preventDefault();
//         if (this.id === "id_pagenumber_left") {
//             if (currentPageNumber !== 1) {
//                 currentPageNumber--;
//                 $("#id_pagenumber_" + currentPageNumber).tab('show');
//             }
//         } else if (this.id === "id_pagenumber_right") {
//             if (currentPageNumber !== maxPageNumber) {
//                 currentPageNumber++;
//                 $("#id_pagenumber_" + currentPageNumber).tab('show');
//             }
//         } else {
//             $(this).tab('show');
//             currentPageNumber = +this.id.replace(/^\D+/g, ''); // id-ic vercnenq eji hamar@
//         }
//     });
// }

//////////___Paginations___//////////

//////////////////////////////  Cart   ///////////////////////////////

    if (docTitle === "Cart") {

        ////   Cart Quantity and Total Price   ////

        //   Cart Total Price (on page load) //

        var cartUnitPrice = $('.cart_unit_price'),
            cartTotalPrice = $('.cart_total_price'),
            cartQuantityInput = $(".cart_quantity_input"),
            totalAmount = $("#id_total_amount"),
            totalAmountValue = 0;

        for (var i = 0; i < cartTotalPrice.length - 1; i++) {
            $(cartTotalPrice[i]).text("$" + ($(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val()));
            totalAmountValue += $(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val();
        }
        $(totalAmount).text("$" + totalAmountValue);
        //__Cart Total Price (on page load)__//

        //  On Quantity change  //

        var flagInputEmptyBlur = false;
        // Input aneluc toxel miayn tver@, chtoxel 99-ic avel lini
        $(cartQuantityInput).on("input", "", function () {
            if ($(this).val() !== "") {
                $(this).val(+$(this).val().replace(/[^\d]/g, ""));
                if ($(this).val() === "0") {
                    $(this).val("");
                } else if (+$(this).val() > 99) {
                    do {
                        $(this).val($(this).val().substring(0, $(this).val().length - 1));
                    } while (+$(this).val() > 99);
                }
            }
            // total-@ poxel
            var totalPrice = "$" + +$(this).parent().prev().text().replace(/[^\d]/g, "") *
                +$(this).val();
            $(this).parent().next().children(":first").text(totalPrice);
            // total amount-@ poxel
            totalAmountValue = 0;
            for (var i = 0; i < cartTotalPrice.length - 1; i++) {
                totalAmountValue += $(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val();
            }
            $(totalAmount).text("$" + totalAmountValue);
            var productId = $(this).parent().attr('data-id');
            if ($(this).val() > 0) {
                ajaxUpdateCart(productId, $(this).val());
            }
        });

        // Inputic heracneluc ete datark e, sarqi 1
        $(cartQuantityInput).on("blur", "", function () {
            if ($(this).val() === "") {
                $(this).val("1");
                flagInputEmptyBlur = true;
            }
            // total-@ poxel
            var totalPrice = "$" + +$(this).parent().prev().text().replace(/[^\d]/g, "") *
                +$(this).parent().children(":nth-child(2)").val();
            $(this).parent().next().children(":first").text(totalPrice);
            // total amount-@ poxel
            totalAmountValue = 0;
            for (var i = 0; i < cartTotalPrice.length - 1; i++) {
                totalAmountValue += $(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val();
            }
            $(totalAmount).text("$" + totalAmountValue);
            var productId = $(this).parent().attr('data-id');
            if ($(this).val() == 1) {
                ajaxUpdateCart(productId, $(this).val());
            }
        });

        // Plus aneluc avelacnel mek hatov, chtoxel 99-ic avel lini
        $('.cart_quantity_plus').on("click", "", function () {
            if (flagInputEmptyBlur === true) {
                flagInputEmptyBlur = false;
            } else if ($(this).prev().val() < 99) {
                $(this).prev()[0].value++;
            }
            // total-@ poxel
            var totalPrice = "$" + +$(this).parent().prev().text().replace(/[^\d]/g, "") *
                +$(this).prev().val();
            $(this).parent().next().children(":first").text(totalPrice);
            // total amount-@ poxel
            totalAmountValue = 0;
            for (var i = 0; i < cartTotalPrice.length - 1; i++) {
                totalAmountValue += $(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val();
            }
            $(totalAmount).text("$" + totalAmountValue);
            var productId = $(this).parent().attr('data-id');
            ajaxUpdateCart(productId, $(this).prev().val());
        });

        // Minus aneluc stugel, ete 1 che, apa 1-ov pakasacnel
        $('.cart_quantity_minus').on("click", "", function () {
            if ($(this).next().val() > 1) {
                $(this).next()[0].value--;
                // total-@ poxel
                var totalPrice = "$" + +$(this).parent().prev().text().replace(/[^\d]/g, "") *
                    +$(this).next().val();
                $(this).parent().next().children(":first").text(totalPrice);
                // total amount-@ poxel
                totalAmountValue = 0;
                for (var i = 0; i < cartTotalPrice.length - 1; i++) {
                    totalAmountValue += $(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val();
                }
                $(totalAmount).text("$" + totalAmountValue);
                var productId = $(this).parent().attr('data-id');
                ajaxUpdateCart(productId, $(this).next().val());
            }
        });

        //__On Quantity change__//

        ////___Cart Quantity and Total Price___////

        ////   Cart Remove   ////

        // $('.cart_remove_button').on("click", "", function () {
        //     $(this).parent().parent().remove();
        //     cartUnitPrice = $('.cart_unit_price');
        //     cartTotalPrice = $('.cart_total_price');
        //     cartQuantityInput = $(".cart_quantity_input");
        //
        //     // ete apranq chka, jnjel total price tox@, hakarak depqum hashvel total price-@
        //     if (cartTotalPrice.length === 1) {
        //         $(".total_price").hide();
        //     } else {
        //         totalAmountValue = 0;
        //         for (var i = 0; i < cartTotalPrice.length - 1; i++) {
        //             $(cartTotalPrice[i]).text("$" + ($(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val()));
        //             totalAmountValue += $(cartUnitPrice[i]).text().replace(/^\D+/g, '') * $(cartQuantityInput[i]).val();
        //         }
        //         $(totalAmount).text("$" + totalAmountValue);
        //     }
        // })

        ////___Cart Remove___////
    }

/////////////////////////////___Cart___/////////////////////////////

//////////   Contact us - phone   //////////

    if (docTitle === "Contact") {
        $("#id_phone").mask("(999) 999-999");
    }

//////////___Contact us - phone___//////////

})
;

function loginRegisterModal(loginOrRegister, errorMessage = "") {

    var loginError = $("#id_loginerror"),
        registerError = $("#id_registererror"),
        loginHeader = $("#id_login_header"),
        registerHeader = $("#id_register_header"),
        loginTab = $("#id_login_tab"),
        registerTab = $("#id_register_tab");

    $('#id_modalloginregister').modal({});
    switch (loginOrRegister) {
        case "login":
            $(loginHeader).addClass("active");
            $(registerHeader).removeClass("active");
            $(loginTab).addClass("active in");
            $(registerTab).removeClass("active in");
            if (errorMessage !== "") {
                $(loginError).removeClass('errorhide');
                $(loginError).html(errorMessage);
            }
            break;
        case "register":
            $(loginHeader).removeClass("active");
            $(registerHeader).addClass("active");
            $(loginTab).removeClass("active in");
            $(registerTab).addClass("active in");
            if (errorMessage !== "") {
                $(registerError).removeClass('errorhide');
                $(registerError).html(errorMessage);
            }
            break;
    }


}

// funkcian veradardznum e objecti arajin handipac ayn elementi index@, vor uni trvac klass@
function getElementIndexByClassname(objectname, classname) {
    for (var i = 0; i < objectname.length; i++) {
        if ($(objectname[i]).hasClass(classname)) {
            return i;
        }
    }
    return -1;
}

// funkcian url-ic stanum e getov uxarkvox parametri arjeq
function getParameterFromUrl(sParam, url) {
    var sPageURL = url.substring(url.indexOf("?") + 1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function updateHashAddress(Object) {
    var hashAddress = "";

    $.each(Object, function (index, value) {
        if (index !== '_token') {
            hashAddress += index + '=' + value + '&';
        }
    });
    hashAddress = hashAddress.slice(0, -1);
    location.hash = hashAddress;
}


//////////   Validation functions   //////////

function validateName(name) {
    var result = true;

    if (name === '') {
        result = 'Name cannot be empty';
    }
    return result;
}

function validateSurname(surname) {
    var result = true;

    if (surname === '') {
        result = 'Surname cannot be empty'
    }
    return result;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (re.test(String(email).toLowerCase())) {
        return true;
    } else {
        return 'This email address is not valid.';
    }
}

function validatePassword(password) {
    var result = true;

    if (password === '') {
        result = 'Password cannot be empty';
    }
    else if ((password.length < 7) || (password.length > 40)) {
        result = 'Password must be between 7 and 40 characters in length';
    }
    else if (password.match("^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{1,}$")) {
        result = 'Password must contain at least one letter and number.';
    }
    else if (password.indexOf(' ') !== -1) {
        result = 'Password cannot contain spaces.';
    }

    return result;
}

function inputFieldValidation(type, field, errorField) {
    var value = $(field).val(),
        validation = '';

    switch (type) {
        case 'name':
            validation = validateName(value);
            break;
        case 'surname':
            validation = validateSurname(value);
            break;
        case 'email':
            validation = validateEmail(value);
            break;
    }

    if (validation === true) {
        $(errorField).html('');
        $(errorField).addClass('errorhide');
        return true;
    } else {
        $(errorField).html('<p> ' + validation + ' </p>');
        $(errorField).removeClass('green');
        $(errorField).removeClass('errorhide');
        return false;
    }
}

function inputPasswordValidation(passwordField, passwordErrorField, passwordConfirmationField, passwordConfirmationErrorField) {
    var value = $(passwordField).val(),
        confirmationValue = $(passwordConfirmationField).val(),
        validation = validatePassword(value),
        result = true;

    if (validation === true) {
        $(passwordErrorField).html('');
        $(passwordErrorField).addClass('errorhide');
    } else {
        $(passwordErrorField).html('<p> ' + validation + ' </p>');
        $(passwordErrorField).removeClass('errorhide');
        result = false;
    }

    if (value === confirmationValue) {
        $(passwordConfirmationErrorField).html('');
        $(passwordConfirmationErrorField).addClass('errorhide');
    } else {
        $(passwordConfirmationErrorField).html('<p> Confirmation does not match </p>');
        $(passwordConfirmationErrorField).removeClass('errorhide');
        result = false;
    }
    return result;
}

function registerValidation(field, validationObject) {
    var fieldId = $(field).attr('id'),
        errorField = $('#' + fieldId + '_error'),
        confirmationErrorField = '';

    switch (fieldId) {
        case 'id_register_name' :
            validationObject ['name'] = inputFieldValidation('name', field, errorField);
            break;
        case  'id_register_surname' :
            validationObject ['surname'] = inputFieldValidation('surname', field, errorField);
            break;
        case  'id_register_email' :
            validationObject ['email'] = inputFieldValidation('email', field, errorField);
            break;
        case  'id_register_password' :
            var confirmationField = $("#id_register_password_confirmation");
            confirmationErrorField = $("#id_register_password_confirmation_error");
            validationObject ['password'] = inputPasswordValidation(field, errorField, confirmationField, confirmationErrorField);
            break;
        case 'id_register_password_confirmation':
            var passwordField = $("#id_register_password");
            confirmationErrorField = errorField;
            errorField = $("#id_register_password_error");
            validationObject ['password'] = inputPasswordValidation(passwordField, errorField, field, confirmationErrorField);
            break;
    }
    return validationObject;
}

//////////___Validation functions___//////////

//////////   Ajax functions   //////////

// TODO: stugel keep_signed_in-@ ashxatum e, te che

function ajaxLogin(email, password, keep_signed_in) {
    $.ajax({
        type: 'post',
        url: $('meta[name="route_login"]').attr('content'),
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "email": $('#id_login_email').val(), // TODO:// dzel , vor email@ funkcian kancheluc ta
            "password": $('#id_login_password').val(), // TODO:// dzel , vor email@ funkcian kancheluc ta
            "keep_signed_in": keep_signed_in
        },
        success: function (data) {
            var info = JSON.parse(data),
                error_field = $("#id_loginerror");
            switch (info['result']) {
                case 'logged_in':
                    reloadPageWithoutHash();
                    break;
                case 'wrong_credentials':
                    $(error_field).html('<p>Wrong username or password</p>');
                    $(error_field).removeClass("errorhide");
                    break;
                case 'verify_email':
                    $("#id_login_tab").replaceWith('<div id="id_login_tab" class="text-center tab-pane fade register_tab active in">\n' +
                        '<p class="verify_message">Please verify your email address</p>\n' +
                        '<a id="id_resend_btn" href="#" type="button" class="btn resend_btn">Resend email</a>\n' +
                        ' </div>\n');

                    $('#id_resend_btn').on('click', function (e) {
                        e.preventDefault();
                        ajaxSendVerificationEmail(info['resend_link']);
                    });
                    break;
                default:
                    console.log('unexpected error');
            }
        }
    });
}

function ajaxRegister(name, surname, email, password, password_confirmation) {
    $.ajax({
        type: 'post',
        url: $('meta[name="route_register"]').attr('content'),
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "name": name,
            "surname": surname,
            "email": email,
            "password": password,
            "password_confirmation": password_confirmation
        },
        success: function (data) {
            var register_tab = $("#id_register_tab");

            switch (data['result']) {
                case 'success':
                    $(register_tab).replaceWith(' ' +
                        '<div id="id_login_tab" class="text-center">\n' +
                        '<p class="verify_message">Please verify your email!</p>\n' +
                        ' </div>\n');
                    setTimeout(function () {
                        reloadPageWithoutHash();
                    }, 4000);
                    break;
                default:
                    console.log('unexpected error');
            }
        }
    });
}

function ajaxSendVerificationEmail(url) {
    $.ajax({
        type: 'get',
        url: url,
        data: {},
        success: function (data) {
            var info = JSON.parse(data),
                login_tab = $("#id_login_tab");
            switch (info['result']) {
                case 'email_sent':
                    $(login_tab).replaceWith(' ' +
                        '<div id="id_login_tab" class="text-center">\n' +
                        '<p class="verify_message">Your email has been sent, please verify</p>\n' +
                        ' </div>\n');
                    setTimeout(function () {
                        reloadPageWithoutHash();
                    }, 4000);
                    break;
                case 'already_verified':
                    $(login_tab).replaceWith(' ' +
                        '<div id="id_login_tab" class="text-center">\n' +
                        '<p class="verify_message">Your email already verified, please login</p>\n' +
                        ' </div>\n');
                    setTimeout(function () {
                        location.reload();
                    }, 4000);
                    break;
                default:
                    console.log('unexpected error');
            }
        }
    })
}

function ajaxSendResetEmail(email, errorField) {
    $.ajax({
        type: 'post',
        url: $('meta[name="route_passwordreset.email"]').attr('content'),
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "email": email
        },
        success: function (data) {
            var login_tab = $("#id_login_tab");

            switch (data['result'][0]) {
                case 'success':
                    $(login_tab).replaceWith(' ' +
                        '<div id="id_login_tab" class="text-center">\n' +
                        '<p class="verify_message">We have e-mailed your password reset link!</p>\n' +
                        ' </div>\n');
                    setTimeout(function () {
                        reloadPageWithoutHash();
                    }, 4000);
                    break;
                case 'user does not exist':
                    $(errorField).html("<p>We can't find a user with that e-mail address.</p>");
                    $(errorField).removeClass("errorhide");
                    break;
                default:
                    console.log('unexpected error');
            }
        }
    });
}

function ajaxResetPassword(email, password) {
    var hashAddress = document.location.hash,
        token = hashAddress.substring(12);
    $.ajax({
        type: 'post',
        url: $('meta[name="route_passwordreset.change"]').attr('content'),
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "email": email,
            "password": password,
            "token": token
        },
        success: function (data) {
            var password_reset_tab = $("#id_password_reset_tab");
            switch (data['result'][0]) {
                case 'success':
                    $(password_reset_tab).replaceWith(' ' +
                        '<div id="id_login_tab" class="text-center">\n' +
                        '<p class="verify_message">Your password has been resetted, please login again</p>\n' +
                        ' </div>\n');
                    setTimeout(function () {
                        window.location.href = "#login";
                        location.reload();
                    }, 4000);
                    break;
                default:
                    $(password_reset_tab).replaceWith(' ' +
                        '<div id="id_login_tab" class="text-center">\n' +
                        '<p class="verify_message">Wrong operation!</p>\n' +
                        ' </div>\n');
                    setTimeout(function () {
                        reloadPageWithoutHash();
                    }, 4000);
                    break;
            }
        }
    });
}

function ajaxEmailCheck(email, email_error_field) {
    $.ajax({
        type: 'post',
        async: false,
        url: $('meta[name="route_checkEmailExists"]').attr('content'),
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "email": email
        },
        success: function (data) {
            var element = $(email_error_field);
            switch (data['result'][0]) {
                case 'success':
                    $(element).html('<p>E-mail is free</p>');
                    $(element).addClass('green');
                    $(element).removeClass('errorhide');
                    return true;
                case 'email exists':
                    $(element).html('<p> The email has already been taken. </p>');
                    $(element).removeClass('green');
                    $(element).removeClass('errorhide');
                    return false;
                default:
                    console.log(data['result'][0]);
                    return false;
            }
        }
    });
}

function ajaxGetProductsPage(htmlArea, url, queryObject) {
    $.ajax({
        type: 'get',
        url: url,
        data: queryObject,
        success: function (data) {
            $(htmlArea).html(data);
        }
    })
}

function ajaxAddToCart(id, quantity) {
    $.ajax({
        type: 'post',
        url: $('meta[name="route_cart.store"]').attr('content'),
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "id": id,
            "quantity": quantity
        },
        success: function (data) {
            var result = JSON.parse(data)['result'];
            var notificationField = $('#fixedNotification');
            switch (result) {
                case 'success':
                    $(notificationField).show();
                    $(notificationField).find('#notificationMessage').html('Product successfully added to your cart');
                    $(notificationField).addClass('alert-success');
                    $(notificationField).removeClass('hidden');
                    setTimeout(function () {
                        $(notificationField).slideUp("slow");
                    }, 4000);
                    break;
                case 'already_in_cart':
                    $(notificationField).show();
                    $(notificationField).find('#notificationMessage').html('Product already in your cart');
                    $(notificationField).addClass('alert-danger');
                    $(notificationField).removeClass('hidden');
                    setTimeout(function () {
                        $(notificationField).slideUp("slow");
                    }, 4000);
                    break;
                default:
                    console.log('unexpected error');
            }
        }
    });
}

function ajaxUpdateCart(id, quantity) {
    $.ajax({
        type: 'post',
        url: $('meta[name="route_cart.store"]').attr('content'),
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "_method": 'PUT',
            "id": id,
            "quantity": quantity
        },
        success: function (data) {
        }
    });
}

/////   Change mouse cursor, when processing axaj requests   /////

$(document).ajaxStart(function () {
    $("body").css({'cursor': 'wait'});
}).ajaxStop(function () {
    $("body").css({'cursor': 'default'});
});

/////___Change mouse cursor, when processing axaj requests___/////

//////////___Ajax functions___//////////

// refresh anel ej@ aranc #-i
function reloadPageWithoutHash() {
    window.location.replace(window.location.href.split('#')[0]);
}

function urlToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        if (!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    }
    return request;
}

function removeFromObjectByKeyPrefix(object, prefix) {
    $.each(object, function (key, value) {
        if (key.startsWith(prefix)) {
            delete object[key];
        }
    });
}

function fillInputsFromObject(inputs, object) {
    $.each(inputs, function (key, input) {
            if ($(input).attr('type') === 'checkbox') {
                $.each(object, function (index, value) {
                    if ($(input).attr('name') === index) {
                        $(input).attr('checked', 'checked');
                    }
                });
            } else if ($(input).attr('name') === 'priceRange' && 'priceRange' in object) {
                $(input).attr('value', object['priceRange']);
                $(input).attr('data-slider-value', '[' + object['priceRange'] + ']');
            }
        }
    );
}