$(function () {
    var docTitle = document.title,
        allInputGroups = $("#id_allInputGroups"),
        pagesArray_IndexAndShowPages = [
            "Categorygroups",
            "Categorygroup show",
            "Categories",
            "Category show",
            "Brands",
            "Brand show",
            "Measurments",
            "Measurment show",
            "Parametergroups",
            "Parametergroup show",
            "Parameters",
            "Parameter show",
            "Images",
            "Image show",
            "Products",
            "Product show"
        ],
        pagesArray_CreatePages = [
            "Add categorygroup",
            "Add category",
            "Add brand",
            "Add measurment",
            "Add parametergroup",
            "Add parameter",
            "Add image"
        ],
        pagesArray_withUriInputFieldPages = [
            "Add categorygroup",
            "Edit categorygroup",
            "Add category",
            "Edit category",
            "Add brand",
            "Edit brand"
        ];

    //////////////////     Index and Show pages     ///////////////////

    if (pagesArray_IndexAndShowPages.includes(docTitle)) {
        $(".delete").on('click', function (e) {
            e.preventDefault();
            var elementsArray = [];
            switch (docTitle) {
                case 'Categorygroups':
                case 'Categorygroup show':
                    elementsArray = getChildElements($(this).attr('data-id'), 'route_categorygroupCategories');
                    console.log(this);
                    deleteModal(this, 'Categorygroup', ' contains these categories. Remove them first.', '', elementsArray, false);
                    break;
                case 'Categories':
                case 'Category show':
                    elementsArray = getChildElements($(this).attr('data-id'), 'route_categoryProducts');
                    deleteModal(this, 'Category', ' contains these products', '*elements will not be deleted', elementsArray, true);
                    break;
                case 'Brands':
                case 'Brand show':
                    elementsArray = getChildElements($(this).attr('data-id'), 'route_brandProducts');
                    deleteModal(this, 'Brand', ' is attached to these products. Detach them first.', '', elementsArray, false);
                    break;
                case 'Measurments':
                case 'Measurment show':
                    elementsArray = getChildElements($(this).attr('data-id'), 'route_measurmentParameters');
                    deleteModal(this, 'Measurment', ' is being used by these parameters. Detach them first.', '', elementsArray, false);
                    break;
                case 'Parametergroups':
                case 'Parametergroup show':
                    elementsArray = getChildElements($(this).attr('data-id'), 'route_parametergroupParameters');
                    deleteModal(this, 'Parametergroup', ' contains these parameters. Remove them first.', '', elementsArray, false);
                    break;
                case 'Parameters':
                case 'Parameter show':
                    elementsArray = getChildElements($(this).attr('data-id'), 'route_parameterProducts');
                    deleteModal(this, 'Parameter', ' is being used by these products.', '*products will not be deleted', elementsArray, true);
                    break;
                case 'Images':
                case 'Image show':
                    deleteModal(this, '', '', '', [], true);
                    break;
                case 'Products':
                case 'Product show':
                    elementsArray = getChildElements($(this).attr('data-id'), 'route_product.carts');
                    if (elementsArray.length > 0) {
                        deleteModal(this, 'Product', " is in these users' cart.", '*after deleteing carts, check again for orders', elementsArray, false);
                    } else {
                        elementsArray = getChildElements($(this).attr('data-id'), 'route_product.orders');
                        deleteModal(this, 'Product', ' is in use by these orders.', '', elementsArray, false);
                    }
                    break;
            }
        });
    }

    ////////////////_____Index and Show pages_____/////////////////

    ///////////////////////     Create pages     ////////////////////////

    if (pagesArray_CreatePages.includes(docTitle)) {

        //////////////    Add new input group    //////////////

        $("#id_addInputGroup").on('click', function () {
            var inputGroupArray = $(".inputGroup"),
                inputGroupArrayLength = inputGroupArray.length,
                inputGroupLastChild = inputGroupArray[inputGroupArrayLength - 1],
                nextInputGroupNumber = +$(inputGroupLastChild).attr('data-inputNumber') + 1,
                newInputGroup = $(inputGroupLastChild).clone();

            newInputGroup.attr("data-inputNumber", nextInputGroupNumber);
            $(allInputGroups).append(newInputGroup);

            var newInputs = newInputGroup.find('input');

            $.each(newInputs, function (index, value) {
                var name = $(value).attr('name'),
                    nameWithoutIndex = name.substr(0, name.indexOf('[')),
                    textAfterIndex = name.substr(name.indexOf(']')),
                    newName = nameWithoutIndex + '[' + nextInputGroupNumber + textAfterIndex;
                $(value).attr('name', newName);
                $(value).val('');
            });

            var newSelects = newInputGroup.find('select');

            $.each(newSelects, function (index, value) {
                var name = $(value).attr('name'),
                    nameWithoutIndex = name.substr(0, name.indexOf('[')),
                    textAfterIndex = name.substr(name.indexOf(']')),
                    newName = nameWithoutIndex + '[' + nextInputGroupNumber + textAfterIndex;
                $(value).attr('name', newName);
                $(value).val(0);
            });
        });

        ///////////____Add new input group____////////////

        ///////////////    Delete input group    ///////////////

        $(allInputGroups).on('click', '.deleteInputGroup', function () {
            if ($(allInputGroups).find('.deleteInputGroup').length > 1) {
                $(this).parent().parent().remove();
            }
        });

        /////////////____Delete input group____/////////////

    }

    /////////////////////_____Create pages_____//////////////////////

    //////////////////////////     Custom     ///////////////////////////

    if (docTitle === "Categorygroups") {

        /////////////////////   Sorting   ///////////////////////

        var categorygroupTable = $("#id_categorygroupTable"),
            categorygroupTableBody = $(categorygroupTable).find("tbody");

        $(categorygroupTableBody).sortable();
        $(categorygroupTableBody).sortable("disable");

        $("#id_changeSortOrder").on('click', function (e) {
            e.preventDefault();

            $(this).html('<i class="fa fa-floppy-o" aria-hidden="true"></i> Save sort order');
            $(this).removeClass('btn-info');
            $(this).addClass('alert-warning');
            $(this).attr('id', 'id_saveSortOrder');

            $(categorygroupTableBody).sortable("enable");

            $("#id_saveSortOrder").on('click', function (e) {
                e.preventDefault();
                var categorygroupTable = $("#id_categorygroupTable"),
                    rowsArray = $(categorygroupTable).find("tbody").find("tr"),
                    newSortOrderForm = $("#id_newSortOrderForm");

                $.each(rowsArray, function (index, value) {
                    $(newSortOrderForm).append(
                        '<input ' +
                        'name="sortValues[' + $(value).attr('data-id') + ']" ' +
                        'value="' + index + '">'
                    );
                });
                $(newSortOrderForm).submit();
            });
        })

        ////////////////////___Sorting___////////////////////

    }

    if (docTitle === "Add image" ||
        docTitle === "Add product"
    ) {
        ///////////////    Ajax send form    ///////////////
        ///////////////    Image required    ///////////////

        $("#saveButton").on('click', function (e) {
            e.preventDefault();
            var allForm = $("#allForm"),
                allFileInputs = $(allForm).find(':file'),
                allFileInputsFilled = true,
                notificationsField = $("#notificationsField");

            $.each(allFileInputs, function (index, value) {
                if (value.files.length === 0) {
                    allFileInputsFilled = false;
                }
            });

            if (allFileInputsFilled === true) {
                var formData = new FormData($(allForm)[0]);
                sendFormDataByAjax($('meta[name="route_store"]').attr('content'),
                    $('meta[name="route_index"]').attr('content'),
                    formData, notificationsField);
            } else {
                $(notificationsField).html('<div class="alert alert-danger alert-dismissable">\n' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">\n' +
                    '<i class="fa fa-times" aria-hidden="true"></i>\n' +
                    '</a>\n' +
                    '<div><p>All input fields are required.</p> </div>\n' +
                    '</div>');
            }
        });
        /////////////____Ajax send form____/////////////
    }

    if (docTitle === "Edit product"
    ) {
        ///////////////    Ajax send form    ///////////////
        /////////////    Image not required    ////////////

        $("#saveButton").on('click', function (e) {
            e.preventDefault();
            var allForm = $("#allForm"),
                notificationsField = $("#notificationsField");


            var formData = new FormData($(allForm)[0]);
            formData.append('_method', 'PUT');

            sendFormDataByAjax($(allForm).attr('action'),
                $('meta[name="route_index"]').attr('content'),
                formData, notificationsField);
        });
        /////////////____Ajax send form____/////////////
    }


    if (docTitle === "Add product" ||
        docTitle === "Edit product") {

        /////    Add and edit optional input fields    /////
        ///////////  categories, parameteres  ///////////

        var categoryQty = $('#layoutCategory').attr('data-qty'),
            parameterQty = +$('#layoutParameter').attr('data-qty');

        $('[data-type="addButton"]').on('click', function () {
            var elementType = $(this).attr('id').replace('add', ''),
                layoutId = $(this).attr('id').replace('add', 'layout'),
                layout = $("#" + layoutId),
                groupElementId = $(this).attr('id').replace('add', 'group'),
                groupElement = $("#" + groupElementId),
                newElement = $(layout).clone(),
                elementQty = 0;

            $(newElement).attr('id', '');
            $(newElement).removeClass('hidden');
            $(groupElement).append(newElement);

            if (elementType === "Category") {
                elementQty = categoryQty;
            } else if (elementType === "Parameter") {
                elementQty = parameterQty;
            }

            var newInputs = newElement.find('input');

            $.each(newInputs, function (index, value) {
                $(value).attr('name', $(value).attr('name') + '[' + elementQty + ']');
            });

            var newSelects = newElement.find('select');

            $.each(newSelects, function (index, value) {
                $(value).attr('name', $(value).attr('name') + '[' + elementQty + ']');
            });

            if (elementType === "Category") {
                categoryQty = ++elementQty;
            } else if (elementType === "Parameter") {
                parameterQty = ++elementQty;
            }
        });
        ///____Add and edit optional input fields____///

        /////////    Delete optional input fields    ////////
        ///////////  categories, parameteres  ///////////

        $('#allForm').on('click', '.deleteGroup', function () {
            $(this).parent().parent().remove();
        });
        //////____Delete optional input fields____///////
    }

    if (docTitle === "Edit product") {
        var deletedImagesId = [];
        $('.deleteImageButton').on('click', function () {
            var deletedId = $(this).attr('data-id');
            deletedImagesId.push(deletedId);
            $("#deletedImagesId").val(deletedImagesId);
            $("#selectMainImage").find('option[value="' + deletedId + '"]').remove();
            $(this).parent().parent().remove();
        })
    }

    if (pagesArray_withUriInputFieldPages.includes(docTitle)) {

        ///////////////    URI field input    ///////////////
        $(allInputGroups).on('blur', 'input[name^=name]', function () {
            var value = $(this).val().toLowerCase().trim(),
                uriField = $(this).parent().parent().find('input[name^=uri]'),
                uriValue = value.replace(/\s/gm, "-").replace(/[^-a-z\d\s:]/gm, "").replace(/-{2,}/gm, '-');
            $(uriField).val(uriValue);
        });
        $(allInputGroups).on('blur', 'input[name^=ur]', function () {
            var uriField = $(this).parent().parent().find('input[name^=uri]'),
                uriValue = $(this).val().toLowerCase().trim().replace(/\s/gm, '-').replace(/[^-a-z\d\s:]/gm, '').replace(/-{2,}/gm, '-');
            $(uriField).val(uriValue);
        });
        /////////////____URI field input____/////////////

    }

    ////////////////////////_____Custom_____/////////////////////////
});

function deleteModal(object, objectType, warningMessageTop = '', warningMessageBottom = '', warningNames = [], allowedDeleteIfNotEmpty = false) {
    var name = $(object).attr('data-deleteName'),
        url = $(object).attr('data-deleteUrl');

    $('#messageTop').html(' Delete entry : <strong>' + name + '</strong>');

    if (warningNames.length > 0) {
        $('#warningMessageTop').text(objectType + warningMessageTop);
        var elements = '';
        $.each(warningNames, function (index, value) {
            elements += '<li>' + value + '</li>'
        });
        $('#warningMessageList').html(elements);
        $('#warningMessageBottom').html('<strong>' + warningMessageBottom + '</strong>');

        if (allowedDeleteIfNotEmpty) {
            $("#allowedDelete").removeClass('hidden');
            $('#forbiddenDelete').addClass('hidden');
        } else {
            $("#allowedDelete").addClass('hidden');
            $('#forbiddenDelete').removeClass('hidden');
        }
    } else {
        $('#warningMessageTop').text('');
        $('#warningMessageList').html('');
        $('#warningMessageBottom').html('');
        $("#allowedDelete").removeClass('hidden');
        $('#forbiddenDelete').addClass('hidden');
    }

    $('#delete_button').attr('action', url);
    $('#deleteModal').modal();
}

function getChildElements(id, routeName) {
    var elements = [];
    $.ajax({
        type: 'get',
        async: false,
        url: $('meta[name="' + routeName + '"]').attr('content') + '/' + id,
        data: {},
        success: function (data) {
            elements = JSON.parse(data);
        }
    });
    return elements;
}

function sendFormDataByAjax(url, urlIfSuccess, formData, notificationsField) {
    $.ajax({
        url: url,
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data: formData,
        dataType: 'json',
        cache: false,
        processData: false,
        contentType: false,
        success: function (data) {
            if (data['result'][0] === 'success') {
                window.location.replace(urlIfSuccess);
            } else {
                var errors = '';
                $.each(data['result'], function (index, value) {
                    errors += '<div><p>' + value + '</p></div>';
                });
                $(notificationsField).html(
                    '<div class="alert alert-danger alert-dismissable">\n' +
                    '<a href="#" class="close" data-dismiss="alert" aria-label="close">\n' +
                    '<i class="fa fa-times" aria-hidden="true"></i>\n' +
                    '</a>\n' +
                    errors +
                    '</div>');
            }
        }
    });
}


