<div id="id_modalloginregister" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content modal_login_register">
            <div class="modal-header modal_header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <ul id="id_login_register_menu" class="nav nav-tabs login_register_menu">
                    <li id="id_login_header" class="active login_register_tab_button"><a href="#id_login_tab">Login</a>
                    </li>
                    <li id="id_register_header" class="login_register_tab_button"><a
                                href="#id_register_tab">Register</a></li>
                </ul>
            </div>
            <div id="id_tab_content" class="tab-content modal-body">

                <div class="tab-pane fade in active login_tab" id="id_login_tab">
                    <form class="form-group" role="form" action="{{route('login')}}" method="POST">
                        <div class="loginerror errorhide" id="id_loginerror">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="id_login_email"
                                   placeholder="Email address" name="email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control input-lg" id="id_login_password"
                                   placeholder="Password" name="password">
                        </div>
                        <input id="id_keep_signed_in" type="checkbox">
                        <label for="id_keep_signed_in">Keep me signed in</label>
                        <a class="pull-right reset_password_link" href="#" id="id_reset_password_link">Reset
                            password</a>
                        <button type="button" class="btn  center-block login_btn" id="id_signin">Sign in</button>
                    </form>
                </div>

                <div class="tab-pane fade register_tab" id="id_register_tab">
                    <form class="form-group" method="POST" id="id_register_form" action="{{route('register')}}">
                        <div class=" form-group">
                            <input type="text" class="form-control input-lg" id="id_register_name"
                            placeholder="Name" name="name">
                        </div>
                        <div class="loginerror errorhide" id="id_register_name_error">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="id_register_surname"
                            placeholder="Surname" name="surname">
                        </div>
                        <div class="loginerror errorhide" id="id_register_surname_error">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="id_register_email"
                            placeholder="Email Address" name="email">
                        </div>
                        <div class="loginerror errorhide" id="id_register_email_error">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control input-lg" id="id_register_password"
                                   placeholder="Password" name="password">
                        </div>
                        <div class="loginerror errorhide" id="id_register_password_error">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control input-lg" id="id_register_password_confirmation"
                                   placeholder="Confirm password" name="password_confirmation">
                        </div>
                        <div class="loginerror errorhide" id="id_register_password_confirmation_error">
                        </div>
                        <button type="submit" class="btn center-block register_btn " id="id_sign_up_button">Sign up</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>