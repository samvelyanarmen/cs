<h3>CATEGORIES</h3>
<div class="panel-group panel_category" id="accordion">
    @foreach($categorygroups as $categorygroup)
        <div class="panel panel-default panel_main">
            <div class="panel-heading panel_heading">
                <h4 class="panel-title">
                    <a href="{{route('front.categorygroup',['categorygroupuri' => $categorygroup->uri])}}"
                       data-linktype="categorygroup">
                        {{$categorygroup->name}}
                    </a>
                    <a data-toggle="collapse" data-parent="#accordion" href="#{{$categorygroup->id}}">
                        <i class="fa fa-plus pull-right" aria-hidden="true"></i>
                    </a>
                </h4>
            </div>
            <div id="{{$categorygroup->id}}" class="panel-collapse collapse
            @if( isset($selectedCategorygroup) && $categorygroup->uri === $selectedCategorygroup->uri)
            {{'in'}}
            @endif
                    ">
                <div class="panel-body panel_brands">
                    <ul>
                        @foreach($categorygroup->categories as $category)
                            <li>
                                <a href="{{route('front.category',['categorygroupuri' => $categorygroup->uri, 'categoryuri' => $category->uri])}}"
                                   data-linktype="category"
                                @if(isset($selectedCategory) && $category->uri === $selectedCategory->uri)
                                    {{'class=bold'}}
                                    @endif
                                >
                                    {{$category->name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endforeach
</div>