<div id="id_modalviewpicture" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" id="id_modal_viewpicture_content">
            <div class="modal-header modal_viewpicture_header" id="id_modal_viewpicture_header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="id_modaltitle"></h4>
            </div>
            <div class="container modal-body modal_viewpicture_body" id="id_modal_viewpicture_body">
                <i class="fa fa-angle-left modal_viewpicture_nav_left"
                   id="id_modal_viewpicture_nav_left"></i>
                <i class="fa fa-angle-right modal_viewpicture_nav_right"
                   id="id_modal_viewpicture_nav_right"></i>
                <img src="" id="id_modalviewpicture_currentimage" alt="Current product image">
            </div>
            <div class="modal-footer modal_viewpicture_footer" id="id_modal_viewpicture_footer">
                <div class="container">
                    <!-- Controls -->
                    <div id="id_owl_in_modal_nav_controls" class=""></div>
                    <div class="owl-carousel owl-theme product_images_carousel" id="id_owl_carousel_in_modal">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>