<h3>BRANDS</h3>
<div class="brands">
    <ul>
        @foreach($brands as $brand)
            <li>
                <a href="#">{{$brand->name}}
                    <span class="badge pull-right">
                        {{$brand->products->count()}}
                    </span>
                </a>
            </li>
        @endforeach
    </ul>
</div>
