@if(count($brands))
    <h4 class="filter_head">Brands</h4>
    @php
        $i=0;
    @endphp
    @foreach($brands as $brand)
        <div class="checkbox">
            <label>
                <input type="checkbox" value="{{$brand->id}}" name="brand[{{$i++}}]">{{$brand->name}}
            </label>
        </div>
    @endforeach
@endif