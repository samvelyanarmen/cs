<div class="container">
    <div class="row">

        <div class="col-md-6 col-md-offset-3" id="notificationsField">
            @if(count($errors))
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>

                @foreach ($errors->all() as $error)
                    <div><p>{{ $error }}</p> </div>
                @endforeach
                </div>
            @endif

            @if(session()->has('error'))
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{--{{session()->get('error')}}--}}
{{--                    {!! session()->get('error') !!}--}}
                </div>
            @endif

            @if(session()->has('info'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! session()->get('info') !!}
                </div>
            @endif
        </div>
    </div>
</div>