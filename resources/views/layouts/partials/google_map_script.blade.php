<script>
    function initMap() {
        var myLatLng = {lat: 40.189562, lng: 44.523454};
        var myLatLng2 = {lat: 41.189562, lng: 44.523454};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng,
            gestureHandling: 'cooperative',
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: true,
            rotateControl: false,
            fullscreenControl: false,
            styles: [
                {
                    "stylers": [
                        {
                            "hue": "#ff1a0a"
                        },
                        {
                            "invert_lightness": true
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 33
                        },
                        {
                            "gamma": 0.5
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#2D333C"
                        }
                    ]
                }
            ]
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Office1'
        });
        var marker2 = new google.maps.Marker({
            position: myLatLng2,
            map: map,
            title: 'Office2'
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDce-MAK41rJP1ND-vnrGl8A6R5KhoBGKw&callback=initMap">
</script>