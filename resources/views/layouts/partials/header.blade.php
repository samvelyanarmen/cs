<header>
    <!-- Top line -->
    <div class="top-line">
        <div class="container">
            <div class="row">
                <div class="col-xs-7 top-line_left">
                    <ul class="list-inline">
                        <li><a href="tel:+37410204050"><i class="fa fa-phone" aria-hidden="true"></i> +374 10 204050</a>
                        </li>
                        <li><a href="mailto:info@domain.com"><i class="fa fa-envelope" aria-hidden="true"></i>
                                info@domain.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-5 top-line_right">
                    <ul class="nav navbar-nav list-inline pull-right">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /Top line -->
    <!-- Service information -->
    <div class="container">
        <div class="row service">
            <div class="col-xs-12 col-md-4">
                <a href="{{route('front')}}"> <img class="logo" src="{{route('front')}}/images/logo.png" alt="logo"> </a>
                <div class="btn-group pull-right clearfix button_usa_dollar">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle myBtn"
                                data-toggle="dropdown">
                            USA
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Canada</a></li>
                            <li><a href="#">UK</a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle myBtn"
                                data-toggle="dropdown">
                            DOLLAR
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Canadian Dollar</a></li>
                            <li><a href="#">Pound</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-8 service_menu">
                <ul class="list-inline pull-right">
                    <li class="cart"><a href="{{route('cart.index')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            Cart</a></li>

                    @auth
                        <li>


                            <div class="btn-group pull-right clearfix button_account">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle myBtn"
                                            data-toggle="dropdown">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        {{  Auth::user()->name }}
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                               document.getElementById('logout-form').submit();">
                                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                                Logout</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </li>
                    @endauth
                    @guest
                        <li><a id="login_button" href="#" class="login_register"><i class="fa fa-sign-in"
                                                                                       aria-hidden="true"></i> Login</a>
                        </li>
                        <li><a id="register_button" href="#" class="login_register"><i class="fa fa-lock"
                                                                                          aria-hidden="true"></i>
                                Register</a>
                        </li>
                    @endguest

                </ul>
            </div>
        </div>
    </div>
    <!-- /Service information -->
    <!-- Category & search -->
    <div class="container category_search" data-spy="affix" data-offset-top="106" id="myNavbar">
        <nav class="navbar navbar-default navbar_main" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header search_div">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navbar_collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <input type="search" class="search_field" placeholder="Search">
                </div>
                <div class="collapse navbar-collapse category" id="navbar_collapse">
                    <ul class="nav navbar-nav">
                        <li class="active custom"><a href="{{route('front')}}">Home</a></li>
                        <li><a href="contact_us.html">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>
    <div class="for_affix_padding"></div>
    <!-- /Category & search -->
</header>