<h3>Filters</h3>
<div class="filters">
    <form action="{{route('front.products')}}" method="GET" id="filterForm">
        {{ csrf_field() }}
        @include('layouts.partials.brands_filter')
        <h4 class="filter_head">Availibility</h4>
        <div class="checkbox">
            <label>
                <input name="availibility" type="checkbox" value="available">
                Available
            </label>
        </div>
        @if($priceMin && $priceMax)
            <div id="priceRangeParent" class="well text-center rangebarParent">
                <input id="priceRange" name="priceRange" type="text" class="span2"
                       value="{{$priceMin}},{{$priceMax}}" data-slider-min="{{$priceMin}}"
                       data-slider-max="{{$priceMax}}"
                       data-slider-step="1" data-slider-value="[{{$priceMin ?: 0}},{{$priceMax ?: 0}}]"/>
                <b class="pull-left" id="priceRangeMin">$ {{$priceMin}}</b> <b class="pull-right"
                                                                               id="priceRangeMax">$ {{$priceMax}}</b>
            </div>
        @endif
        @foreach($parameters as $parameter)
            <h4 class="filter_head">{{$parameter->name}}</h4>
            @php
                $i=0;
                $values =json_decode($parameter->pivot->valuearray);
                asort($values);
            @endphp
            @foreach($values as $value)
                <div class="checkbox">
                    <label>
                        <input name="parameter#{{$parameter->id}}[{{$i++}}]" type="checkbox" value="{{$value}}">
                        @switch($parameter->measurment->name)
                            @case('MB')
                            @if($value >= 1024)
                                {{$value/1024 .' GB' }}
                            @else
                                {{$value.' '.$parameter->measurment->name }}
                            @endif
                            @break
                            @case('null')
                            {{$value}}
                            @break
                            @default
                            {{$value.' '.$parameter->measurment->name }}
                        @endswitch
                    </label>
                </div>
            @endforeach
        @endforeach
    </form>
</div>