{{--TODO: jnjel avelord fielder@ formayic--}}
<div id="id_modalpasswordreset" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content modal_login_register">
            <div class="modal-header modal_header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <ul class="nav nav-tabs password_reset_menu">
                    <li id="id_login_header" class="active login_register_tab_button"><a href="#reset">Reset Password</a>
                </ul>
            </div>
            <div id="id_tab_content" class="tab-content modal-body">
                <div class="tab-pane fade in active password_reset_tab" id="id_password_reset_tab">
                    <form class="form-group" method="POST" id="id_password_reset_form">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="id_reset_email"
                                   placeholder="Email Address" name="email" data-result="">
                        </div>
                        <div class="loginerror errorhide" id="id_reset_email_error">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control input-lg" id="id_reset_password"
                                   placeholder="Password" name="password" data-result="">
                        </div>
                        <div class="loginerror errorhide" id="id_reset_password_error">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control input-lg" id="id_reset_password_confirmation"
                                   placeholder="Confirm password" name="password_confirmation" data-result="">
                        </div>
                        <div class="loginerror errorhide" id="id_reset_password_confirmation_error">
                        </div>
                        <button type="submit" class="btn center-block reset_btn " id="id_reset_button">Change password
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{--</div>--}}