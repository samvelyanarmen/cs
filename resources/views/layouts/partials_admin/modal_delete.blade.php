<div class="modal fade" id="deleteModal" role="dialog">
    <div class="modal-dialog modal-sm modalDelete">
        <!-- Modal content-->
        <div class="modal-content">
            <!-- Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirm</h4>
            </div>
            <!-- Modal body-->
            <div class="modal-body">
                <h4 class="modal-title text-center" id="messageTop"></h4>
                <br>
                <p id="warningMessageTop" class="text-center bold"></p>

                <ul id="warningMessageList" class="alert-danger">
                </ul>
                <p id="warningMessageBottom" class="modal-title"></p>
            </div>
            <!-- Modal footer-->
            <div class="modal-footer ">
                <div id="allowedDelete">
                    <form id="delete_button" method="POST" style="display: inline;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger">Yes</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
                <div id="forbiddenDelete" class="hidden">
                    <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>