@extends('layouts.app')
@section('title','Add parameter')

@section('content')
    <div class="col-xs-12">
        <h1>Add parameter</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <button type="button" class="btn btn-success" id="id_addInputGroup">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
        <form action="{{ route('parameter.store') }}" method="post">
            <div class="row" id="id_allInputGroups">
                {{ @csrf_field() }}
                @if(old())
                    @foreach(old('name') as $key => $value)
                        <div class="col-xs-12 inputGroup" data-inputNumber="{{$key}}">
                            <div class="row">
                                <div class="form-group col-xs-3 ">
                                    <input name="name[{{$key}}]" value="{{old('name.'.$key)}}" type="text"
                                           class="form-control"
                                           placeholder="Parameter name">
                                </div>
                                <div class="form-group col-xs-2">
                                    <select name="type[{{$key}}]" class="form-control">
                                        <option value="0">Choose type</option>
                                        <option value="text"
                                        @if(old('type.'.$key) == 'text')
                                            {{'selected'}}
                                                @endif
                                        >text
                                        </option>
                                        <option value="number"
                                        @if(old('type.'.$key) == 'number')
                                            {{'selected'}}
                                                @endif
                                        >
                                            number
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-2">
                                    <select name="measurment_id[{{$key}}]" class="form-control">
                                        <option value="0">Choose measurment</option>
                                        @foreach($measurments as $measurment)
                                            <option value="{{$measurment->id}}"
                                            @if(old('measurment_id.'.$key) == $measurment->id)
                                                {{'selected'}}
                                                    @endif
                                            >
                                                {{$measurment->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-xs-3">
                                    <select name="parametergroup_id[{{$key}}]" class="form-control">
                                        <option value="0">Choose parametergroup</option>
                                        @foreach($parametergroups as $parametergroup)
                                            <option value="{{$parametergroup->id}}"
                                            @if(old('parametergroup_id.'.$key) == $parametergroup->id)
                                                {{'selected'}}
                                                    @endif
                                            >
                                                {{$parametergroup->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="button" class="btn btn-danger deleteInputGroup">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    @endforeach

                @else
                    <div class="col-xs-12 inputGroup" data-inputNumber="1">
                        <div class="row">
                            <div class="form-group col-xs-3">
                                <input name="name[1]" type="text" class="form-control"
                                       placeholder="Parameter name">
                            </div>
                            <div class="form-group col-xs-2">
                                <select name="type[1]" class="form-control">
                                    <option value="0">Choose type</option>
                                    <option value="text">text</option>
                                    <option value="number">number</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-2">
                                <select name="measurment_id[1]" class="form-control">
                                    <option value="0">Choose measurment</option>
                                    @foreach($measurments as $measurment)
                                        <option value="{{$measurment->id}}">
                                            {{$measurment->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-3 ">
                                <select name="parametergroup_id[1]" class="form-control">
                                    <option value="0">Choose parametergroup</option>
                                    @foreach($parametergroups as $parametergroup)
                                        <option value="{{$parametergroup->id}}">{{$parametergroup->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="button" class="btn btn-danger deleteInputGroup">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                @endif

            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    Save
                </button>
            </div>
        </form>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('parameter.index') }}"><strong>All
                    Parameters</strong></a>
        </div>
    </div>
@endsection