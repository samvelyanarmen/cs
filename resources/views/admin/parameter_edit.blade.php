@extends('layouts.app')
@section('title','Edit parameter')

@section('content')
    <div class="col-xs-12">
        <h1>Edit parameter</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('parameter.update',$parameter->id) }}" method="post">
            <div class="row">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="col-xs-12">
                    <div class="row">
                        <div class="form-group col-xs-3 ">
                            <input name="name" type="text" id="parameter" class="form-control"
                                   value="{{$parameter->name}}" placeholder="Parameter name">
                        </div>
                        <div class="form-group col-xs-2">
                            <select name="type" class="form-control">
                                <option value="0">Choose type</option>
                                <option value="text"
                                @if($parameter->type == 'text')
                                    {{'selected'}}
                                        @endif
                                >
                                    text
                                </option>
                                <option value="number"
                                @if($parameter->type == 'number')
                                    {{'selected'}}
                                        @endif
                                >number</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-2">
                            <select name="measurment_id" class="form-control">
                                <option value="0">Choose measurment</option>
                                @foreach($measurments as $measurment)
                                    <option value="{{$measurment->id}}"
                                    @if($parameter->measurment_id == $measurment->id)
                                        {{'selected'}}
                                            @endif
                                    >
                                        {{$measurment->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-3">
                            <select name="parametergroup_id" class="form-control">
                                <option value="0">Choose parametergroup</option>
                                @foreach($parametergroups as $parametergroup)
                                    <option value="{{$parametergroup->id}}"
                                    @if($parameter->parametergroup_id == $parametergroup->id)
                                        {{'selected'}}
                                            @endif
                                    >
                                        {{$parametergroup->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-small btn-success "><strong>STORE</strong></button>
                    </div>
                    <div class="form-group">
                        <a class="btn btn-small btn-primary" href="{{ route('parameter.index') }}"><strong>All
                                Parameters</strong></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection