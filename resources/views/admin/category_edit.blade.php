@extends('layouts.app')
@section('title','Edit category')

@section('content')
    <div class="col-xs-12">
        <h1>Edit category</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('category.update',$category->id) }}" method="post">
            <div class="row">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="col-xs-12">
                    <div class="row" id="id_allInputGroups" >
                        <div class="form-group col-xs-3 ">
                            <input name="name" type="text" id="category" class="form-control"
                                   value="{{$category->name}}" placeholder="Category name">
                        </div>
                        <div class="form-group col-xs-3 ">
                            <input name="uri" type="text" class="form-control"
                                   value="{{$category->uri}}" placeholder="URI">
                        </div>
                        <div class="form-group col-xs-3">
                            <select name="categorygroup_id" class="form-control">
                                <option value="0">Choose categorygroup</option>
                                @foreach($categorygroups as $id => $name)
                                    <option value="{{$id}}"
                                    @if($category->categorygroup_id == $id)
                                        {{'selected'}}
                                            @endif
                                    >
                                        {{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-small btn-success "><strong>STORE</strong></button>
                    </div>
                    <div class="form-group">
                        <a class="btn btn-small btn-primary" href="{{ route('category.index') }}"><strong>All
                                Categories</strong></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection