@extends('layouts.app')
@section('title','Image show')
@section('routes')
    {{--<meta name="route_categoryProducts" content="{{route('category.products','')}}">--}}
@endsection

@section('content')
    <div class="col-xs-12">
        <h1>Image: "{{$image->name}}"</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_imageTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">Id</th>
                <th class="text-center">Name</th>
                <th class="text-center">Product</th>
                <th class="text-center">Main</th>
                <th class="text-center">Size</th>
                <th class="text-center">Created at</th>
                <th class="text-center">Updated at</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($image) && is_object($image))
                <tr data-id="{{$image->id}}">
                    <td> {{$image->id}}</td>
                    <td> {{$image->name}}</td>
                    <td>{{$image->product->name}}</td>
                    <td>{{$image->main}}</td>
                    <td>{{$image->size}}</td>
                    <td> {{$image->created_at}}</td>
                    <td> {{$image->updated_at}}</td>
                    <td class="text-center">
                        <a href="{{route('image.edit', $image->id)}}"
                           data-toggle="tooltip" title="Update image" class="btn btn-info btn-sm" role="button">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="#" data-toggle="tooltip" title="Delete image"
                           class="btn btn-danger btn-sm delete"
                           role="button" data-deleteName="{{$image->name}}"
                           data-deleteUrl="{{route('image.destroy',$image->id)}}"
                           data-id="{{$image->id}}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>

        @if(isset($image) && is_object($image))
            <div class="row">
                <div class="form-group col-xs-8 col-sm-4">
                    <img src="{{route('front').'/'. $image->src}}" alt="{{$image->product->name}}"
                         class="img-responsive">
                </div>
            </div>
        @endif
        <div class="form-group">
            <a href="{{route('image.create')}}" class="btn btn-success" role="button">
                <i class="fa fa-plus-square-o" aria-hidden="true"></i>
                Add image
            </a>
        </div>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('image.index') }}"><strong>All
                    Images</strong></a>
        </div>
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection