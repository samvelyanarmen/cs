@extends('layouts.app')
@section('title','Add image')
@section('routes')
    <meta name="route_store" content="{{route('image.store','')}}">
    <meta name="route_index" content="{{route('image.index','')}}">
@endsection

@section('content')
    <div class="col-xs-12">
        <h1>Add image</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <button type="button" class="btn btn-success" id="id_addInputGroup">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
        <form action="{{ route('image.store') }}" method="post" enctype="multipart/form-data" id="allForm">
            <div class="row" id="id_allInputGroups">
                {{ @csrf_field() }}
                <div class="col-xs-12 inputGroup" data-inputNumber="1">
                    <div class="row">
                        <div class="form-group col-xs-3">
                            <input name="productfiles[1][]" type="file" class="form-control" id="test"
                                   placeholder="Image name" multiple value="0">
                        </div>
                        <div class="form-group col-xs-3 ">
                            <select name="product_id[1]" class="form-control">
                                <option value="0">Choose product</option>
                                @foreach($products as $id => $name)
                                    <option value="{{$id}}">{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="button" class="btn btn-danger deleteInputGroup">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success" id="saveButton">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    Save
                </button>
            </div>
        </form>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('image.index') }}"><strong>All
                    Images</strong></a>
        </div>
    </div>
@endsection