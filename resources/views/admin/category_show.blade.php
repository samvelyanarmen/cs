@extends('layouts.app')
@section('title','Category show')
@section('routes')
    <meta name="route_categoryProducts" content="{{route('category.products','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>Category: "{{$category->name}}"</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_categoryTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">Id</th>
                <th class="text-center">Name</th>
                <th class="text-center">URI</th>
                <th class="text-center">Categorygroup</th>
                <th class="text-center">Products</th>
                <th class="text-center">Created at</th>
                <th class="text-center">Updated at</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($category) && is_object($category))
                    <tr data-id="{{$category->id}}">
                        <td> {{$category->id}}</td>
                        <td> {{$category->name}}</td>
                        <td> {{$category->uri}}</td>
                        <td>{{$category->categorygroup->name}}</td>
                        <td>
                            @foreach($category->products as $product)
                                <p>{{$product->name}}</p>
                            @endforeach
                        </td>
                        <td> {{$category->created_at}}</td>
                        <td> {{$category->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('category.edit', $category->id)}}"
                               data-toggle="tooltip" title="Update category" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete category"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$category->name}}"
                               data-deleteUrl="{{route('category.destroy',$category->id)}}"
                               data-id="{{$category->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
            @endif
            </tbody>
        </table>
        <div class="form-group">
        <a href="{{route('category.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add category
        </a>
    </div>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('category.index') }}"><strong>All
                    Categories</strong></a>
        </div>
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection