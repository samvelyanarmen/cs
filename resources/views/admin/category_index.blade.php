@extends('layouts.app')
@section('title','Categories')
@section('routes')
    <meta name="route_categoryProducts" content="{{route('category.products','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All categories</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('category.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add category
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_categoryTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">@sortablelink('uri','URI')</th>
                <th class="text-center">@sortablelink('categorygroup.name','Categorygroup')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($categories) && is_object($categories))
                @foreach($categories as $category)
                    <tr data-id="{{$category->id}}">
                        <td> {{$category->id}}</td>
                        <td> {{$category->name}}</td>
                        <td> {{$category->uri}}</td>
                        <td> {{$category->categorygroup->name}}</td>
                        <td> {{$category->created_at}}</td>
                        <td> {{$category->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('category.show', $category->id)}}"
                               data-toggle="tooltip" title="Show category" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('category.edit', $category->id)}}"
                               data-toggle="tooltip" title="Update category" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete category"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$category->name}}"
                               data-deleteUrl="{{route('category.destroy',$category->id)}}"
                               data-id="{{$category->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! $categories->appends(\Request::except('page'))->render() !!}
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection