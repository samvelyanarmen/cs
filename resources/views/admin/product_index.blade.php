@extends('layouts.app')
@section('title','Products')
@section('routes')
    <meta name="route_product.carts" content="{{route('product.carts','')}}">
    <meta name="route_product.orders" content="{{route('product.orders','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All products</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('product.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add product
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_productTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">@sortablelink('quantity','Qantity')</th>
                <th class="text-center">@sortablelink('price','Price')</th>
                <th class="text-center">@sortablelink('brand.name','Brand')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($products) && is_object($products))
                @foreach($products as $product)
                    <tr data-id="{{$product->id}}">
                        <td> {{$product->id}}</td>
                        <td> {{$product->name}}</td>
                        <td> {{$product->quantity}}</td>
                        <td> {{$product->price}}</td>
                        <td> {{$product->brand->name}}</td>
                        <td> {{$product->created_at}}</td>
                        <td> {{$product->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('product.show', $product->id)}}"
                               data-toggle="tooltip" title="Show product" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('product.edit', $product->id)}}"
                               data-toggle="tooltip" title="Update product" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete product"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$product->name}}"
                               data-deleteUrl="{{route('product.destroy',$product->id)}}"
                               data-id="{{$product->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! $products->appends(\Request::except('page'))->render() !!}

    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection