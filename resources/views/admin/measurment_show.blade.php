@extends('layouts.app')
@section('title','Measurment show')
@section('routes')
    <meta name="route_measurmentParameters" content="{{route('measurment.parameters','')}}">
@endsection


@section('content')

    <div class="col-xs-12">
        <h1>Measurment: "{{$measurment->name}}"</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_measurmentTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">Id</th>
                <th class="text-center">Name</th>
                <th class="text-center">Parameters</th>
                <th class="text-center">Created at</th>
                <th class="text-center">Updated at</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($measurment) && is_object($measurment))
                    <tr data-id="{{$measurment->id}}">
                        <td> {{$measurment->id}}</td>
                        <td> {{$measurment->name}}</td>
                        <td>
                            @foreach($measurment->parameters as $parameter)
                                <p>{{$parameter->name}}</p>
                            @endforeach
                        </td>
                        <td> {{$measurment->created_at}}</td>
                        <td> {{$measurment->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('measurment.edit', $measurment->id)}}"
                               data-toggle="tooltip" title="Update measurment" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete measurment"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$measurment->name}}"
                               data-deleteUrl="{{route('measurment.destroy',$measurment->id)}}"
                               data-id="{{$measurment->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
            @endif
            </tbody>
        </table>
        <div class="form-group">
        <a href="{{route('measurment.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add measurment
        </a>
    </div>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('measurment.index') }}"><strong>All
                    Measurments</strong></a>
        </div>
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection