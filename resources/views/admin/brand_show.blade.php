@extends('layouts.app')
@section('title','Brand show')
@section('routes')
    <meta name="route_brandProducts" content="{{route('brand.products','')}}">
@endsection


@section('content')
    <div class="col-xs-12">
        <h1>Brand: "{{$brand->name}}"</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_brandTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">Id</th>
                <th class="text-center">Name</th>
                <th class="text-center">URI</th>
                <th class="text-center">Products</th>
                <th class="text-center">Created at</th>
                <th class="text-center">Updated at</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($brand) && is_object($brand))
                    <tr data-id="{{$brand->id}}">
                        <td> {{$brand->id}}</td>
                        <td> {{$brand->name}}</td>
                        <td> {{$brand->uri}}</td>
                        <td>
                            @foreach($brand->products as $product)
                                <p>{{$product->name}}</p>
                            @endforeach
                        </td>
                        <td> {{$brand->created_at}}</td>
                        <td> {{$brand->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('brand.edit', $brand->id)}}"
                               data-toggle="tooltip" title="Update brand" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete brand"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$brand->name}}"
                               data-deleteUrl="{{route('brand.destroy',$brand->id)}}"
                               data-id="{{$brand->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
            @endif
            </tbody>
        </table>
        <div class="form-group">
        <a href="{{route('brand.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add brand
        </a>
    </div>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('brand.index') }}"><strong>All
                    Brands</strong></a>
        </div>
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection