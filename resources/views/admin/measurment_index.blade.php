@extends('layouts.app')
@section('title','Measurments')
@section('routes')
    <meta name="route_measurmentParameters" content="{{route('measurment.parameters','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All measurments</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('measurment.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add measurment
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_measurmentTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($measurments) && is_object($measurments))
                @foreach($measurments as $measurment)
                    <tr data-id="{{$measurment->id}}">
                        <td> {{$measurment->id}}</td>
                        <td> {{$measurment->name}}</td>
                        <td> {{$measurment->created_at}}</td>
                        <td> {{$measurment->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('measurment.show', $measurment->id)}}"
                               data-toggle="tooltip" title="Show measurment" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('measurment.edit', $measurment->id)}}"
                               data-toggle="tooltip" title="Update measurment" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete measurment"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$measurment->name}}"
                               data-deleteUrl="{{route('measurment.destroy',$measurment->id)}}"
                               data-id="{{$measurment->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! $measurments->appends(\Request::except('page'))->render() !!}

    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection