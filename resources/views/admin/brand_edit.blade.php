@extends('layouts.app')
@section('title','Edit brand')

@section('content')
    <div class="col-xs-12">
        <h1>Edit brand</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('brand.update',$brand->id) }}" method="post">
            <div class="row">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="col-xs-12">
                    <div class="row" id="id_allInputGroups">
                        <div class="form-group col-xs-3 ">
                            <input name="name" type="text" id="brand" class="form-control"
                                   value="{{$brand->name}}" placeholder="Brand name">
                        </div>
                        <div class="form-group col-xs-3 ">
                            <input name="uri" type="text" class="form-control"
                                   value="{{$brand->uri}}" placeholder="URI">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-small btn-success "><strong>STORE</strong></button>
                    </div>
                    <div class="form-group">
                        <a class="btn btn-small btn-primary" href="{{ route('brand.index') }}"><strong>All
                                Brands</strong></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection