@extends('layouts.app')
@section('title','Product show')
@section('routes')
    <meta name="route_product.carts" content="{{route('product.carts','')}}">
    <meta name="route_product.orders" content="{{route('product.orders','')}}">
@endsection

@section('content')
    <div class="col-xs-12">
        <h1>Product: "{{$product->name}}"</h1>
        <hr>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_productTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">@sortablelink('quantity','Qantity')</th>
                <th class="text-center">@sortablelink('price','Price')</th>
                <th class="text-center">@sortablelink('brand_id.name','Brand')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($product) && is_object($product))
                <tr data-id="{{$product->id}}">
                    <td> {{$product->id}}</td>
                    <td> {{$product->name}}</td>
                    <td> {{$product->quantity}}</td>
                    <td> {{$product->price}}</td>
                    <td> {{$product->brand->name}}</td>
                    <td> {{$product->created_at}}</td>
                    <td> {{$product->updated_at}}</td>
                    <td class="text-center">
                        <a href="{{route('product.edit', $product->id)}}"
                           data-toggle="tooltip" title="Update product" class="btn btn-info btn-sm" role="button">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="#" data-toggle="tooltip" title="Delete product"
                           class="btn btn-danger btn-sm delete"
                           role="button" data-deleteName="{{$product->name}}"
                           data-deleteUrl="{{route('product.destroy',$product->id)}}"
                           data-id="{{$product->id}}">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-3">
                <table class="table table-bordered table-hover table-responsive"
                       id="id_categorygroupCategoriesTable">
                    <thead>
                    <tr class="text-nowrap">
                        <th class="text-center">Categories</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categorygroupCategories as $categorygroup => $categories)
                        <tr>
                            <td><strong>{{$categorygroup}}</strong></td>
                        </tr>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-xs-4">
                <table class="table table-bordered table-hover table-responsive"
                       id="id_categorygroupCategoriesTable">
                    <thead>
                    <tr class="text-nowrap">
                        <th class="text-center" colspan="2">Parameters</th>
                    </tr>
                    <tr>
                        <th>Parameter</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($parametergroupParameters as $parametergroup => $parameters)
                        <tr>
                            <td colspan="2"><strong>{{$parametergroup}}</strong></td>
                        </tr>
                        @foreach($parameters as $parameter)
                            <tr>
                                <td>{{$parameter['name']}}</td>
                                <td>{{$parameter['value']}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xs-12" id="productImages">
        <div class="row">
            @foreach($product->images as $image)
                <div class="col-xs-2 productImageInShowParent">
                    <div class="productImageInShow
                    @if($image->main == 1)
                    {{'main'}}
                    @endif
                            ">
                        <img src="{{route('front').'/'. $image->src}}" alt="{{$image->name}}">
                    </div>
                    <a href="{{route('front').'/'. $image->src}}" class="text-center">
                        {{$image->name}}</a>
                </div>
            @endforeach
        </div>
        <input type="hidden" name="deletedImagesId" id="deletedImagesId">
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('product.index') }}"><strong>All
                    Products</strong></a>
        </div>
    </div>
@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection