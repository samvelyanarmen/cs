@extends('layouts.app')
@section('title','Parametergroups')
@section('routes')
    <meta name="route_parametergroupParameters" content="{{route('parametergroup.parameters','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All parametergroups</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('parametergroup.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add parametergroup
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_parametergroupTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($parametergroups) && is_object($parametergroups))

                @foreach($parametergroups as $parametergroup)
                    <tr data-id="{{$parametergroup->id}}">
                        <td> {{$parametergroup->id}}</td>
                        <td> {{$parametergroup->name}}</td>
                        <td> {{$parametergroup->created_at}}</td>
                        <td> {{$parametergroup->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('parametergroup.show', $parametergroup->id)}}"
                               data-toggle="tooltip" title="Update parametergroup" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('parametergroup.edit', $parametergroup->id)}}"
                               data-toggle="tooltip" title="Update parametergroup" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete parametergroup"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$parametergroup->name}}"
                               data-deleteUrl="{{route('parametergroup.destroy',$parametergroup->id)}}"
                               data-id="{{$parametergroup->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! $parametergroups->appends(\Request::except('page'))->render() !!}

    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection

@section('js_plugins')
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
@endsection