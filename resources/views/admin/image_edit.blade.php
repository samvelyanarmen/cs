@extends('layouts.app')
@section('title','Edit image')

@section('content')
    <div class="col-xs-12">
        <h1>Edit image</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('image.update',$image->id) }}" method="post">
            <div class="row">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="col-xs-12">
                    <div class="row">
                        <div class="form-group col-xs-3 ">
                            <input name="name" type="text" id="image" class="form-control"
                                   value="{{$image->name}}" placeholder="Image name">
                        </div>
                        <div class="form-group col-xs-3 ">
                            <select name="product_id" class="form-control">
                                <option value="0">Choose product</option>
                                @foreach($products as $id => $name)
                                    <option value="{{$id}}"
                                    @if($image->product_id == $id)
                                        {{'selected'}}
                                            @endif
                                    >{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-small btn-success "><strong>STORE</strong></button>
                    </div>
                    <div class="form-group">
                        <a class="btn btn-small btn-primary" href="{{ route('image.index') }}"><strong>All
                                Images</strong></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection