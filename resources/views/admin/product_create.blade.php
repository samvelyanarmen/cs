@extends('layouts.app')
@section('title','Add product')
@section('routes')
    <meta name="route_store" content="{{route('product.store','')}}">
    <meta name="route_index" content="{{route('product.index','')}}">
@endsection

@section('content')
    <div class="col-xs-12">
        <h1>Add product</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data" id="allForm">
            <div class="row" id="id_allInputGroups">
                {{ @csrf_field() }}
                <div class="col-xs-12 inputGroup" data-inputNumber="1">
                    <div class="row">
                        <div class="form-group col-xs-3">
                            <input name="name" type="text" class="form-control"
                                   placeholder="Product name">
                        </div>
                        <div class="form-group col-xs-2">
                            <input name="quantity" type="number" class="form-control"
                                   placeholder="Quantity">
                        </div>
                        <div class="form-group col-xs-2">
                            <input name="price" type="number" class="form-control"
                                   placeholder="Price">
                        </div>
                        <div class="form-group col-xs-2 ">
                            <select name="brand_id" class="form-control">
                                <option value="0">Choose brand</option>
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-xs-3">
                            <input name="productFiles[]" type="file" class="form-control" id="test"
                                   placeholder="Image name" multiple value="0">
                        </div>
                        <div class="form-group col-xs-6 col-xs-offset-right-6">
                            <label for="descriptionTextarea">
                                Description
                            </label>
                            <textarea name="description" id="descriptionTextarea"
                                      class="form-control descriptionTextarea" rows="3"></textarea>
                        </div>
                        <div class="col-xs-4">
                            <div class="row" id="groupCategory">
                                <div class="form-group col-xs-3 col-xs-offset-right-9">
                                    <button type="button" class="btn btn-success" id="addCategory"
                                            data-type="addButton">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        Add category
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="row" id="groupParameter">
                                <div class="form-group col-xs-3 col-xs-offset-right-9" id="addParameter"
                                     data-type="addButton">
                                    <button type="button" class="btn btn-success">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        Add parameter
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success" id="saveButton">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    Save
                </button>
            </div>
        </form>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('product.index') }}"><strong>All
                    Products</strong></a>
        </div>
    </div>
    <div id="layoutCategory" class="hidden" data-qty="1">
        <div class="form-group col-xs-9">
            <select name="category_id" class="form-control">
                <option value="0">Choose category</option>
                @foreach($categorygroups as $categorygroup)
                    <optgroup label="{{$categorygroup->name}}">
                        @foreach($categorygroup->categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        <div class="form-group col-xs-3">
            <button type="button" class="btn btn-danger deleteGroup">
                <i class="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <div id="layoutParameter" class="hidden" data-qty="1">
        <div class="form-group col-xs-6">
            <select name="parameter_id" class="form-control">
                <option value="0">Choose parameter</option>
                @foreach($parametergroups as $parametergroup)
                    <optgroup label="{{$parametergroup->name}}">
                        @foreach($parametergroup->parameters as $parameter)
                            <option value="{{$parameter->id}}">{{$parameter->name}}
                                @if($parameter->measurment->name !== 'null')
                                    {{'('.$parameter->measurment->name.')'}}
                                @endif
                            </option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        <div class="form-group col-xs-4">
            <input name="value" type="text" class="form-control"
                   placeholder="Value">
        </div>
        <div class="form-group col-xs-2">
            <button type="button" class="btn btn-danger deleteGroup">
                <i class="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
    </div>
@endsection