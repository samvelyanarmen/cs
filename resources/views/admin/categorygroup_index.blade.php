@extends('layouts.app')
@section('title','Categorygroups')
@section('routes')
    <meta name="route_categorygroupCategories" content="{{route('categorygroup.categories','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All categorygroups</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('categorygroup.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add categorygroup
        </a>
        <a href="{{route('categorygroup.create')}}" class="btn btn-primary pull-right" id="id_changeSortOrder"
           role="button">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            Change sort order
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_categorygroupTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">#</th>
                <th class="text-center">Name</th>
                <th class="text-center">URI</th>
                <th class="text-center">Created at</th>
                <th class="text-center">Updated at</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($categorygroups) && is_object($categorygroups))
                @php
                    $i=1 ;
                @endphp
                @foreach($categorygroups as $categorygroup)
                    <tr data-id="{{$categorygroup->id}}">
                        <td> {{$i++}}</td>
                        <td> {{$categorygroup->name}}</td>
                        <td> {{$categorygroup->uri}}</td>
                        <td> {{$categorygroup->created_at}}</td>
                        <td> {{$categorygroup->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('categorygroup.show', $categorygroup->id)}}"
                               data-toggle="tooltip" title="Update categorygroup" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('categorygroup.edit', $categorygroup->id)}}"
                               data-toggle="tooltip" title="Update categorygroup" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete categorygroup"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$categorygroup->name}}"
                               data-deleteUrl="{{route('categorygroup.destroy',$categorygroup->id)}}"
                               data-id="{{$categorygroup->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    </div>
    <div class="hidden">
        <form action="{{route('categorygroup.updateSortOrder')}}" method="POST" id="id_newSortOrderForm">
            {{ csrf_field() }}
        </form>
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection

@section('js_plugins')
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
@endsection
