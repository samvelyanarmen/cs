@extends('layouts.app')
@section('title','Categorygroup show')
@section('routes')
    <meta name="route_categorygroupCategories" content="{{route('categorygroup.categories','')}}">
@endsection


@section('content')

    <div class="col-xs-12">
        <h1>Categorygroup: "{{$categorygroup->name}}"</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_categorygroupTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">Id</th>
                <th class="text-center">Name</th>
                <th class="text-center">URI</th>
                <th class="text-center">Categories</th>
                <th class="text-center">Created at</th>
                <th class="text-center">Updated at</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($categorygroup) && is_object($categorygroup))
                    <tr data-id="{{$categorygroup->id}}">
                        <td> {{$categorygroup->id}}</td>
                        <td> {{$categorygroup->name}}</td>
                        <td> {{$categorygroup->uri}}</td>
                        <td>
                            @foreach($categorygroup->categories as $category)
                                <p>{{$category->name}}</p>
                            @endforeach
                        </td>
                        <td> {{$categorygroup->created_at}}</td>
                        <td> {{$categorygroup->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('categorygroup.edit', $categorygroup->id)}}"
                               data-toggle="tooltip" title="Update categorygroup" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete categorygroup"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$categorygroup->name}}"
                               data-deleteUrl="{{route('categorygroup.destroy',$categorygroup->id)}}"
                               data-id="{{$categorygroup->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
            @endif
            </tbody>
        </table>
        <div class="form-group">
        <a href="{{route('categorygroup.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add categorygroup
        </a>
    </div>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('categorygroup.index') }}"><strong>All
                    Categorygroups</strong></a>
        </div>
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection