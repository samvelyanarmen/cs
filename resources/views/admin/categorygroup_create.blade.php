@extends('layouts.app')
@section('title','Add categorygroup')

@section('content')
    <div class="col-xs-12">
        <h1>Add categorygroup</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <button type="button" class="btn btn-success" id="id_addInputGroup">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>
        <form action="{{ route('categorygroup.store') }}" method="post">
            <div class="row" id="id_allInputGroups">
                {{ @csrf_field() }}
                @if(old())

                    @foreach(old('name') as $key => $value)
                        <div class="col-xs-12 inputGroup" data-inputNumber="{{$key}}">
                            <div class="row">
                                <div class="form-group col-xs-3 ">
                                    <input name="name[{{$key}}]" value="{{old('name.'.$key)}}" type="text"
                                           class="form-control"
                                           placeholder="Categorygroup name">
                                </div>
                                <div class="form-group col-xs-3 ">
                                    <input name="uri[{{$key}}]" value="{{old('uri.'.$key)}}" type="text" class="form-control"
                                           placeholder="URI">
                                </div>
                                <button type="button" class="btn btn-danger deleteInputGroup">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    @endforeach

                @else
                    <div class="col-xs-12 inputGroup" data-inputNumber="1">
                        <div class="row">
                            <div class="form-group col-xs-3 ">
                                <input name="name[1]" type="text" class="form-control"
                                       placeholder="Categorygroup name">
                            </div>
                            <div class="form-group col-xs-3 ">
                                <input name="uri[1]" type="text" class="form-control"
                                       placeholder="URI">
                            </div>
                            <button type="button" class="btn btn-danger deleteInputGroup">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                @endif


            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    Save
                </button>
            </div>
        </form>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('categorygroup.index') }}"><strong>All
                    Categorygroups</strong></a>
        </div>
    </div>
@endsection