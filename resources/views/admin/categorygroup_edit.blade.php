@extends('layouts.app')
@section('title','Edit categorygroup')

@section('content')
    <div class="col-xs-12">
        <h1>Edit categorygroup</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('categorygroup.update',$categorygroup->id) }}" method="post">
            <div class="row">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="col-xs-12">
                    <div class="row" id="id_allInputGroups">
                        <div class="form-group col-xs-3 ">
                            <input name="name" type="text" id="categorygroup" class="form-control"
                                   value="{{$categorygroup->name}}" placeholder="Categorygroup name">
                        </div>
                        <div class="form-group col-xs-3 ">
                            <input name="uri" type="text" class="form-control"
                                   value="{{$categorygroup->uri}}" placeholder="URI">
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-small btn-success "><strong>STORE</strong></button>
                    </div>

                    <div class="form-group">
                        <a class="btn btn-small btn-primary" href="{{ route('categorygroup.index') }}"><strong>All
                                Categorygroups</strong></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection