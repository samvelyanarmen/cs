@extends('layouts.app')
@section('title','Parametergroup show')
@section('routes')
        <meta name="route_parametergroupParameters" content="{{route('parametergroup.parameters','')}}">
@endsection


@section('content')

    <div class="col-xs-12">
        <h1>Parametergroup: "{{$parametergroup->name}}"</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_parametergroupTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">Id</th>
                <th class="text-center">Name</th>
                <th class="text-center">Categories</th>
                <th class="text-center">Created at</th>
                <th class="text-center">Updated at</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($parametergroup) && is_object($parametergroup))
                    <tr data-id="{{$parametergroup->id}}">
                        <td> {{$parametergroup->id}}</td>
                        <td> {{$parametergroup->name}}</td>
                        <td>
                            @foreach($parametergroup->parameters as $parameter)
                                <p>{{$parameter->name}}</p>
                            @endforeach
                        </td>
                        <td> {{$parametergroup->created_at}}</td>
                        <td> {{$parametergroup->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('parametergroup.edit', $parametergroup->id)}}"
                               data-toggle="tooltip" title="Update parametergroup" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete parametergroup"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$parametergroup->name}}"
                               data-deleteUrl="{{route('parametergroup.destroy',$parametergroup->id)}}"
                               data-id="{{$parametergroup->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
            @endif
            </tbody>
        </table>
        <div class="form-group">
        <a href="{{route('parametergroup.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add parametergroup
        </a>
    </div>
        <div class="form-group">
            <a class="btn btn-small btn-primary" href="{{ route('parametergroup.index') }}"><strong>All
                    Parametergroups</strong></a>
        </div>
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection