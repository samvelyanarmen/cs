@extends('layouts.app')
@section('title','Images')
@section('routes')
    {{--<meta name="route_categoryProducts" content="{{route('category.products','')}}">--}}
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All images</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('image.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add image
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_imageTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">Image</th>
                <th class="text-center">@sortablelink('product.name','Product')</th>
                <th class="text-center">@sortablelink('main','Main')</th>
                <th class="text-center">@sortablelink('size','Size')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($images) && is_object($images))
                @foreach($images as $image)
                    <tr data-id="{{$image->id}}">
                        <td> {{$image->id}}</td>
                        <td> {{$image->name}}</td>
                        <td><img src="{{route('front').'/'. $image->src}}" alt="{{$image->product->name}}" class="productImage"></td>
                        <td>{{$image->product->name}}</td>
                        <td>
                            @if($image->main == true)
                                {{'yes'}}
                                @else
                                {{'no'}}
                            @endif
                        </td>
                        <td>{{$image->size}}</td>
                        <td> {{$image->created_at}}</td>
                        <td> {{$image->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('image.show', $image->id)}}"
                               data-toggle="tooltip" title="Show image" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('image.edit', $image->id)}}"
                               data-toggle="tooltip" title="Update image" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete image"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$image->name}}"
                               data-deleteUrl="{{route('image.destroy',$image->id)}}"
                               data-id="{{$image->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! $images->appends(\Request::except('page'))->render() !!}

    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection