@extends('layouts.app')
@section('title','Parameters')
@section('routes')
    <meta name="route_parameterProducts" content="{{route('parameter.products','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All parameters</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('parameter.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add parameter
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_parameterTable">
            <tr class="text-nowrap">
            <tr>
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">@sortablelink('type','Type')</th>
                <th class="text-center">@sortablelink('measurment.name','Measurment')</th>
                <th class="text-center">@sortablelink('parametergroup.name','Parametergroup')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($parameters) && is_object($parameters))
                @foreach($parameters as $parameter)
                    <tr data-id="{{$parameter->id}}">
                        <td> {{$parameter->id}}</td>
                        <td> {{$parameter->name}}</td>
                        <td> {{$parameter->type}}</td>
                        <td> {{$parameter->measurment->name}}</td>
                        <td> {{$parameter->parametergroup->name}}</td>
                        <td> {{$parameter->created_at}}</td>
                        <td> {{$parameter->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('parameter.show', $parameter->id)}}"
                               data-toggle="tooltip" title="Show parameter" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('parameter.edit', $parameter->id)}}"
                               data-toggle="tooltip" title="Update parameter" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete parameter"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$parameter->name}}"
                               data-deleteUrl="{{route('parameter.destroy',$parameter->id)}}"
                               data-id="{{$parameter->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>

                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! $parameters->appends(\Request::except('page'))->render() !!}

    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection