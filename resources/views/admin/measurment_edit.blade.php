@extends('layouts.app')
@section('title','Edit measurment')

@section('content')
    <div class="col-xs-12">
        <h1>Edit measurment</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <form action="{{ route('measurment.update',$measurment->id) }}" method="post">
            <div class="row">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="col-xs-12">
                    <div class="row">
                        <div class="form-group col-xs-3 ">
                            <input name="name" type="text" id="measurment" class="form-control"
                                   value="{{$measurment->name}}" placeholder="Measurment name">
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-small btn-success "><strong>STORE</strong></button>
                    </div>

                    <div class="form-group">
                        <a class="btn btn-small btn-primary" href="{{ route('measurment.index') }}"><strong>All
                                Measurments</strong></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection