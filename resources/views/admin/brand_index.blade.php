@extends('layouts.app')
@section('title','Brands')
@section('routes')
    <meta name="route_brandProducts" content="{{route('brand.products','')}}">
@endsection

@section('content')

    <div class="col-xs-12">
        <h1>All brands</h1>
        <hr>
    </div>
    <div class="col-xs-12">
        <a href="{{route('brand.create')}}" class="btn btn-success" role="button">
            <i class="fa fa-plus-square-o" aria-hidden="true"></i>
            Add brand
        </a>
        <br>
        <br>
    </div>
    <div class="col-xs-12">
        <table class="table table-bordered table-hover table-responsive" id="id_brandTable">
            <thead>
            <tr class="text-nowrap">
                <th class="text-center">@sortablelink('id','Id')</th>
                <th class="text-center">@sortablelink('name','Name')</th>
                <th class="text-center">@sortablelink('uri','URI')</th>
                <th class="text-center">@sortablelink('created_at','Created at')</th>
                <th class="text-center">@sortablelink('updated_at', 'Updated at')</th>
                <th class="text-center">Show</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($brands) && is_object($brands))
                @foreach($brands as $brand)
                    <tr data-id="{{$brand->id}}">
                        <td> {{$brand->id}}</td>
                        <td> {{$brand->name}}</td>
                        <td> {{$brand->uri}}</td>
                        <td> {{$brand->created_at}}</td>
                        <td> {{$brand->updated_at}}</td>
                        <td class="text-center">
                            <a href="{{route('brand.show', $brand->id)}}"
                               data-toggle="tooltip" title="Show brand" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-window-maximize" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{route('brand.edit', $brand->id)}}"
                               data-toggle="tooltip" title="Update brand" class="btn btn-info btn-sm" role="button">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" data-toggle="tooltip" title="Delete brand"
                               class="btn btn-danger btn-sm delete"
                               role="button" data-deleteName="{{$brand->name}}"
                               data-deleteUrl="{{route('brand.destroy',$brand->id)}}"
                               data-id="{{$brand->id}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        {!! $brands->appends(\Request::except('page'))->render() !!}
    </div>

@endsection
@section('modals')
    @include('layouts.partials_admin.modal_delete')
@endsection