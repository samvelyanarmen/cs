@extends('layouts.front')

@section('title','Cart')
@section('pageName','Cart')

@section('routes')
    <meta name="route_cart.update" content="{{route('cart.update')}}">
@endsection

@section('content')
    <div class="table-responsive cart_table_div">
        <table class="table ">
            <thead>
            <tr class="cart_head">
                <th>Item</th>
                <th></th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td class="cart_image">
                        <a href="{{$product->src}}">
                            <img src="{{route('front').'/'. $product->images->sortbydesc('main')->first()->src}}"
                                 alt="{{$product->name}}"
                                 class="img-responsive"></a>
                    </td>
                    <td class="cart_description">
                        <a href="{{$product->src}}">{{$product->name}}</a>
                        <p>Web ID: #{{$product->id}}</p>
                    </td>
                    <td class="cart_price">
                        <p class="cart_unit_price">$ {{$product->price}}</p>
                    </td>
                    <td class="cart_quantity" data-id="{{$product->id}}">
                        <button type="button" class="cart_quantity_minus">-</button>
                        <input class="form-control cart_quantity_input" type="text"
                               value="{{$product->pivot->quantity}}"
                               autocomplete="off">
                        <button type="button" class="cart_quantity_plus">+</button>
                    </td>
                    <td class="cart_total">
                        <p class="cart_total_price"></p>
                        <button class="btn btn-default">BUY</button>
                    </td>
                    <td class="cart_remove">
                        <form action="{{route('cart.destroy',$product->id)}}" method="POST" style="display: inline;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="hidden" name="id" value="$product->id">
                            <button class="btn btn-default cart_remove_button" type="submit" data-id="{{$product->id}}">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach


            <tr class="total_price">
                <td>
                    <p>Total Price</p>
                </td>
                <td class="cart_description">

                </td>
                <td class="cart_price">

                </td>
                <td class="cart_quantity">

                </td>
                <td class="cart_total">
                    <p class="cart_total_price" id="id_total_amount"></p>
                    <button class="btn btn-default">BUY ALL</button>
                </td>
                <td class="cart_remove">

                </td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection