@extends('layouts.front')

@section('title','Home')
@section('pageName','Home')

@section('css_plugins')
    <link rel="stylesheet" href="{{asset('css/bootstrap-slider.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-touch-slider.css')}}" media="all">
@endsection

@section('routes')
    <meta name="route_front.products" content="{{route('front.products')}}">
@endsection

@section('carousel')
    @include('layouts.partials.carousel')
@endsection

@section('content')
    {{--<button id="testbutton">TEST</button>--}}
    <div class="col-xs-12 col-sm-3 sidebar">
        <aside>
            @include('layouts.partials.categories')
            @include('layouts.partials.brands')
        </aside>
    </div>
    <!-- Main Information -->
    <div class="col-xs-12 col-sm-9 main_information">
        <div id="productsField">

        </div>
        <!-- Recommended items -->
    @include('layouts.partials.recommended_items')
    <!-- /Recommended items -->
    </div>
    <!-- /Main Information-->
@endsection

@section('modals')
    @include('layouts.partials.password_reset_modal')
@endsection

@section('js_plugins')
    <script src="{{asset('js/bootstrap-slider.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-touch-slider-min.js')}}"></script>
@endsection