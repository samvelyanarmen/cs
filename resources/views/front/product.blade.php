@extends('layouts.front')

@section('title',$product->name)
@section('pageName','Product')

@section('css_plugins')
    <link rel="stylesheet" href="{{asset('css/bootstrap-slider.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-touch-slider.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
@endsection

@section('routes')
    <meta name="route_cart.store" content="{{route('cart.store')}}">
@endsection

@section('content')

@include('layouts.partials.notification_fixed')

    <div class="col-xs-12 col-sm-3 sidebar">
        <aside>
            @include('layouts.partials.categories')
        </aside>
    </div>

    <div class="col-xs-12 col-sm-9 main_information">
        <div class="row">
            <div class="col-xs-12 col-sm-5">
                <div id="id_viewcurrentproduct" class="view_product middle">
                    <img src="{{route('front').'/'. $images[0]->src}}">
                </div>
                <!-- Controls -->
                <div id="id_owl_nav_controls" class="owl-nav"></div>
                <!-- Owl carousel-->
                <div class="owl-carousel owl-theme product_images_carousel" id="id_owl_carousel_product_images">
                    @foreach($images as $image)
                        <div class="item"><img src="{{route('front').'/'. $image->src}}"></div>
                    @endforeach
                </div>
                <!-- /Owl carousel-->
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="product_information">
                    <h5 id="id_productname">{{$product->name}}</h5>
                    <p class="product_web_id">Web ID: #{{$product->id}}</p>
                    <p class="product_price">US $ {{$product->price}}</p>
                    <label class="" for="id_product_details_quantity"> Quantity: </label>
                    <input type="text" class="form-control" id="id_product_details_quantity" value="1"
                           autocomplete="off">
                    <button id="addToCart" data-productId="{{$product->id}}" type="button"
                            class="btn btn-default btn_add_to_cart"
                            @auth data-auth="authenticated" @endauth
                            @guest data-auth="guest" @endguest >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to cart
                    </button>
                    <p><b>Availability:</b>{{$product->quantity ? 'Available' : 'Not available'}}</p>
                    <p><b>Brand:</b> {{$product->brand->name}}</p>
                    <a href="#"><img src="{{route('front')}}/images/icon_facebook.png" alt="facebook"></a>
                    <a href="#"><img src="{{route('front')}}/images/icon_tweet.png" alt="tweet"></a>
                    <a href="#"><img src="{{route('front')}}/images/icon_pin_it.png" alt="pin it"></a>
                    <a href="#"><img src="{{route('front')}}/images/icon_share.png" alt="share"></a>
                </div>
            </div>
        </div>

        @include('layouts.partials.recommended_items')
    </div>

@endsection

@section('modals')
    @include('layouts.partials.view_picture_modal')
@endsection

@section('js_plugins')
    <script src="{{asset('js/bootstrap-slider.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-touch-slider-min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery.ez-plus.js')}}"></script>
    <script src="{{asset('js/swipe.js')}}"></script>
@endsection