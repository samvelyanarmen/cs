@extends('layouts.front')

@section('title','Contact us')

@section('content')
    <div class="contact_us_main_information">
        <div class="col-xs-12">
            <h3>CONTACT US</h3>
            <div id="map" class="contact_us_map"></div>
        </div>
        <div class="col-xs-12 col-sm-8 get_in_touch">
            <h3 class="text-center">Get in touch</h3>
            <form class="form-group" role="form">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="id_name" placeholder="Name">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <input type="email" class="form-control input-lg" id="id_email" placeholder="Email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="id_phone" placeholder="Phone number">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" id="id_subject" placeholder="Subject">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control input-lg" rows="5" placeholder="Your Message Here"></textarea>
                </div>
                <button type="button" class="btn pull-right submit_btn">Submit</button>
            </form>
        </div>
        <div class="col-xs-12 col-sm-4 contact_info_social_networking">
            <h3 class="text-center">Contact Info</h3>
            <address>
                <p>Core-Systems Inc.</p>
                <p>795 Folsom Ave, Suite 600</p>
                <p>San Francisco, CA 94107</p>
                <p>Mobile: +2344 12 47 15</p>
                <p>Fax: 1-17415-411-11</p>
                <p>Email: info@Core-Systems.com</p>
            </address>
            <h3 class="text-center">Social Networking</h3>
            <ul class="list-inline text-center">
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
@endsection

@section('js_plugins')
    <script src="{{asset('js/jquery.maskedinput.min.js')}}"></script>
@endsection