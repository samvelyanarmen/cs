<h3>PRODUCTS</h3>
<div class="row">
    <div class="form-group col-xs-5 col-sm-3 col-md-2 col-lg-2">
        <label>
            Sort by
            <select name="categorygroup_id" class="form-control" data-filter="sort">
                <option value="name" @if($sort== 'name') {{'selected'}}@endif>name</option>
                <option value="price" @if($sort== 'price') {{'selected'}}@endif>price</option>
                <option value="brand.name" @if($sort== 'brand.name') {{'selected'}}@endif>brand</option>
            </select>
        </label>
        @if($order == 'desc')
            <a href="#" title="Change sort order to ascending" data-order="asc"><i class="fa fa-arrow-up"
                                                                                   aria-hidden="true"></i></a>
        @else
            <a href="#" title="Change sort order to descending" data-order="desc"><i class="fa fa-arrow-down"
                                                                                     aria-hidden="true"></i></a>
        @endif
    </div>

    <div class="form-group col-xs-5 col-sm-3 col-md-2 col-lg-2">
        <label>
            Items on page
            <select name="categorygroup_id" class="form-control" data-filter="qtyOnPage">
                @for($i=4; $i<=24; $i+=4)
                    <option value="{{$i}}"
                    @if($qtyOnPage== $i)
                        {{
                'selected'}}
                            @endif
                    >
                        {{$i}}</option>
                @endfor
            </select>
        </label>
    </div>
</div>
<div class="row">
    @if(count($products))
        @foreach($products as $product)
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="product">
                    <div class="text-center only_product">
                        <div class="only_product_image">
                            <a href="{{route('front.product',$product->id)}}"><img src="{{route('front').'/'. $product->images[0]->src }}" alt="product1"></a>
                        </div>
                        <p>${{$product->price}}</p>
                        <a href="{{route('front.product',$product->id)}}">{{$product->name}}</a>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="col-xs-12">
            <h2 class=" text-danger text-center">SORRY, NO PRODUCTS FOUND</h2>
        </div>
    @endif
</div>
{!! $products->appends(\Request::except('page'))->render() !!}
