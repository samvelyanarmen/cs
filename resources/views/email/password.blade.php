Click on this button to reset your password

<a style="
display: inline-block;
padding: 7px 20px;
background-color: dodgerblue;
color: #fff;
outline: none;
text-decoration: none;
border-radius: 5px;"
   href="{{route('front').'#resettoken='.$resetToken}}">
    Reset password
</a>