<?php

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role']], function () {
    Route::get('/', function () {
        return view('home');
    })->name('admin-panel');
    Route::post('/categorygroup/updatesortorder', 'CategorygroupController@updateSortOrder')->name('categorygroup.updateSortOrder');
    Route::get('/categorygroup/categoriesInCategorygroup/{id}', 'CategorygroupController@categoriesInCategorygroup')->name('categorygroup.categories');
    Route::resource('/categorygroup', 'CategorygroupController');
    Route::get('/category/productsInCategory/{id}', 'CategoryController@productsInCategory')->name('category.products');
    Route::resource('/category', 'CategoryController');
    Route::get('/brand/productsInBrand/{id}', 'BrandController@productsInBrand')->name('brand.products');
    Route::resource('/brand', 'BrandController');
    Route::get('/measurment/parametersInMeasurment/{id}', 'MeasurmentController@parametersInMeasurment')->name('measurment.parameters');
    Route::resource('/measurment', 'MeasurmentController');
    Route::get('/parametergroup/parametersInParametergroup/{id}', 'ParametergroupController@parametersInParametergroup')->name('parametergroup.parameters');
    Route::resource('/parametergroup', 'ParametergroupController');
    Route::get('/parameter/productsInParameter/{id}', 'ParameterController@productsInParameter')->name('parameter.products');
    Route::resource('/parameter', 'ParameterController');
    Route::resource('/image', 'ImageController');
    Route::get('/product/cartsOfProduct/{id}', 'ProductController@cartsOfProduct')->name('product.carts');
    Route::get('/product/ordersOfProduct/{id}', 'ProductController@ordersOfProduct')->name('product.orders');
    Route::resource('/product', 'ProductController');
});
