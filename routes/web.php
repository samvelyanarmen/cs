<?php

// TODO: Stugel, ete group-um grum enq middleware => auth, petq e ardyoq amen mi rout-i hamar el grel ?

//User dashboard
Route::group(['middleware' => 'auth'], function () {
    Route::get('/cart', 'CartController@index')->name('cart.index');
    Route::post('/cart', 'CartController@store')->name('cart.store');
    Route::put('/cart', 'CartController@update')->name('cart.update');
    Route::delete('/cart/{id}', 'CartController@destroy')->name('cart.destroy');
    Route::get('/order', 'OrderController@index')->name('front.order');
});
//User dashboard

// Front routes
Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'FrontController@index')->name('front');
    Route::get('/products', 'ProductController@index')->name('front.products'); // For ajax
    Route::get('/product/{productId}', 'ProductController@show')->name('front.product');
    Route::get('/brand/{brandUri}', 'BrandController@index')->name('front.brand');
    Route::get('/{categorygroupUri}', 'CategorygroupController@index')->name('front.categorygroup');
    Route::get('/{categorygroupUri}/{categoryUri}', 'CategoryController@index')->name('front.category');
    Route::get('/contact', 'ContactController')->name('contact');
    Route::post('/checkEmailExists', 'Auth\RegisterController@checkEmailExists')->name('checkEmailExists');
    Route::post('/pass/email', 'Auth\ResetPasswordController@sendPasswordResetEmail')->name('passwordreset.email');
    Route::post('/pass/reset', 'Auth\ResetPasswordController@resetPassword')->name('passwordreset.change');
    Route::get('/verify/token/{token}', 'Auth\VerificationController@verify')->name('auth.verify');
    Route::get('/verify/resend', 'Auth\VerificationController@resend')->name('auth.verify.resend');
    Route::auth();
    Auth::routes();
});

// Temporary route for inserting information in tables
Route::get('/temp', 'TempController@insert');
Route::post('/tempajax', 'TempController@ajax');
