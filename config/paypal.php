<?php

return [
    'client_id' => env('PAYPAL_CLIENT_ID', 'ATmFgpq6sp2vo8TBEBu82oaDWMSsALPp3fcNQBbY0SN1RlVs57zMU91kh7yII__e9LE-8g4t9tKbUHqY'),
    'secret' => env('PAYPAL_SECRET', 'ENNo4ccCHjSyampL3lrglb8rnMmneQkGEVbkz2fltxnrsyQfloW_fSqd--UB_gbLuDFb5uUFCdFOKQiR'),
    'settings' => [
        'mode' => env('PAYPAL_MODE', 'sandbox'),
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.Filename' => storage_path().'/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
    ]
];