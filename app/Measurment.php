<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Measurment extends Model
{
    use Sortable;

    protected $fillable = ['name'];
    public $sortable = ['id', 'name', 'created_at', 'updated_at'];

    public function parameters()
    {
        return $this->hasMany(Parameter::class);
    }
}
