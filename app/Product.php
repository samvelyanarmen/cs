<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    use Sortable;
    protected $fillable = ['name', 'quantity', 'price', 'brand_id', 'rating', 'description'];
    public $sortable = ['id', 'name', 'quantity', 'price', 'brand', 'rating', 'created_at', 'updated_at'];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function parameters()
    {
        return $this->belongsToMany(Parameter::class)->withPivot('value');
    }

    public function carts()
    {
        return $this->belongsToMany(User::class,'carts')->withPivot('quantity');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withPivot('quantity','product_price');
    }
}
