<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Categorygroup;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index($categorygroupUri, $categoryUri)
    {
        $selectedCategorygroup = Categorygroup::where('uri', $categorygroupUri)->first();
        $selectedCategory = Category::where('uri', $categoryUri)->first();

//        TODO: stugel ete Categorygroup@ kam Categoryn goyutyun chunen,
//          TODO:  kam ete Category-n chi patkanum Categorygroup-in, uxarkel 404 ej

        $productsId = $selectedCategory->products->pluck('id')->toArray();
        $brands = Brand::brandsWithProducts($productsId);
        $parameters = $selectedCategory->parameters()->with(['parametergroup', 'measurment'])->get()->sortby('parametergroup.name');
        $priceMin = $selectedCategory->products->min('price');
        $priceMax = $selectedCategory->products->max('price');

        return view('front.category', compact(
            'selectedCategory',
            'selectedCategorygroup',
            'brands',
            'parameters',
            'priceMin',
            'priceMax'
        ));
    }

}
