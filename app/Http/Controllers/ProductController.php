<?php

namespace App\Http\Controllers;

use App\Category;
use App\Categorygroup;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request)
    {

        if (isset($request->categoryId)) {
            $products = Category::find($request->categoryId)->products()->with('images');
        } elseif (isset($request->categorygroupId)) {
            $products = Categorygroup::find($request->categorygroupId)->productsInCategorygroup()->with('images');
        } else {
            $products = Product::with('images');
        }


        $products = $products->distinct('products.id');

        ///////////////  Availibility  ///////////////

        if ($request->availibility === 'available') {
            $products = $products->where('products.quantity', '!=', 0);
        }

        ///////////////  Availibility  ///////////////


        ///////////////  Price  /////////////// ???????????????????????????????????????

        if (isset($request->priceRange)) {
            $needlePosition = strpos($request->priceRange, ',');
            $priceMin = substr($request->priceRange, 0, $needlePosition);
            $priceMax = substr($request->priceRange, $needlePosition + 1);
            $products = $products->where('products.price', '>=', $priceMin)
                ->where('products.price', '<=', $priceMax);
        }

        ///////////////__Price__///////////////??????????????????????????????????????

        ///////////////  Brands  ///////////////

        if ($request->brand) {
            $brandIds = $request->brand;

            $products = $products->where(function ($query) use ($brandIds) {
                $first = false;
                foreach ($brandIds as $brandId) {
                    if ($first === false) {
                        $query = $query->where('brand_id', '=', $brandId);
                        $first = true;
                    } else {
                        $query = $query->orwhere('brand_id', '=', $brandId);
                    }
                }
            });
        }

        ///////////////__Brands__///////////////

        ///////////////  Parameters  ///////////////
        $prefix = 'parameter#';

        foreach ($request->all() as $requestName => $valueArray) {
            if (strpos($requestName, $prefix) === 0) {
                $parameterId = substr($requestName, \strlen($prefix));
                $products = $products->leftJoin('parameter_product as ' . $parameterId, $parameterId . '.product_id', '=', 'products.id');
            }
        }

        foreach ($request->all() as $requestName => $valueArray) {
            if (strpos($requestName, $prefix) === 0) {
                $parameterId = substr($requestName, \strlen($prefix));
                $products = $products->where($parameterId . '.parameter_id', '=', $parameterId);
            }
        }

        foreach ($request->all() as $requestName => $valueArray) {
            if (strpos($requestName, $prefix) === 0) {
                $parameterId = substr($requestName, \strlen($prefix));

                $products = $products->where(function ($query) use ($valueArray, $parameterId) {
                    $first = false;
                    foreach ($valueArray as $value) {
                        if ($first === false) {
                            $query = $query->where($parameterId . '.value', 'LIKE', $value);
                            $first = true;
                        } else {
                            $query = $query->orwhere($parameterId . '.value', 'LIKE', $value);
                        }
                    }
                });
            }
        }

        ///////////////__Parameters__///////////////

        ///////////////  Sorting and pagination  ///////////////

        $sort = $request->sort ?? 'name';
        $order = $request->order ?? 'asc';
        $qtyOnPage = $request->qtyOnPage ?? '12';

        $products = $products->sortable([$sort => $order])->paginate($qtyOnPage);

        ///////////////__Sorting and pagination__///////////////

        return view('front.partials.products', compact('products', 'sort', 'order', 'qtyOnPage'));
    }

    public function show($id)
    {
        $product = Product::find($id);
        $images = $product->images()->orderBy('main','desc')->get();

        return view('front.product',compact(['product','images']));
    }


}
