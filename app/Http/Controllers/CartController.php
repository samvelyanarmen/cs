<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $products = $user->carts()->with('images')->get();

        return view('front.cart', compact('products'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $hasProductInCart = $user->carts()->where('id', $request->id)->exists();

        if (!$hasProductInCart) {

            $data[$request->id] = [
                'quantity' => $request->quantity,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $user->carts()->syncWithoutDetaching($data);

            return json_encode(['result' => 'success']);
        }

        return json_encode(['result' => 'already_in_cart']);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'quantity' => 'required|integer',
            'id' => 'required|exists:products,id'
        ]);
        $data[$request->id] = [
            'quantity' => $request->quantity,
            'updated_at' => Carbon::now(),
        ];
        Auth::user()->carts()->syncWithoutDetaching($data);

        return redirect()->route('categorygroup.index');
    }

    public function destroy($id)
    {
        Auth::user()->carts()->detach($id);
        return back();
    }
}
