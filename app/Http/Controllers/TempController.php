<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Categorygroup;
use App\Category;
use App\Brand;
use App\Measurment;
use App\Parametergroup;
use App\Parameter;
use App\Product;
use App\Image;
use App\User;
use Illuminate\Support\Facades\Storage;
use App\VerificationToken;

class TempController extends Controller
{
    public function insert()
    {
//      dump(  Storage::delete('public/7/cars_2.jpg'));

        /////////////////// Add elements in tables ///////////////////

//        $this->users();
//        $this->categorygroups();
//        $this->categories();
//        $this->brands();
//        $this->measurments();
//        $this->parametergroups();
//        $this->parameters();
//        $this->products();
//        $this->parameter_product();
//        $this->images();

        ///////////////////////////////////////////////////////////////


        ////////////// Create verification token for user /////////////

//        User::where('id', 1)->update(['verified' => 0]);
//        VerificationToken::create([
//            'user_id' => 1,
//            'token' => 'gagasdgaga',
//        ]);
//        echo 'Account resetted';

        ///////////////////////////////////////////////////////////////


        ///////////////////////// Delete user /////////////////////////

//        User::where('email', 'testarm@mail.ru')->delete();
//        echo 'Account testarm@mail.ru deleted';

        ///////////////////////////////////////////////////////////////

    }

    public function users()
    {
        $users = [
            [
                'name' => 'Armen',
                'surname' => 'Samvelyan',
                'email' => 'samvelyanarmen@mail.ru',
                'password' => '$2y$10$0tJKQ.yn4Ntcooxdu.Qr5u8afZFREhVg3WUVTUPt0nTv4cZ84SwDC',
                'remember_token' => 'MUXVy2BwgcYeZ90hph72PVWsjzkNDL9P3JECDIP4eKHTLbunXjfZNIb8lSll',
                'verified' => 1,
                'role' => 1,
                'blocked' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        User::insert($users);

    }

    public function categorygroups()
    {
        $categorygroups = [
            [
                'name' => 'other',
                'sort' => 5,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Computers and monoblocks',
                'sort' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Servers',
                'sort' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Network components',
                'sort' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Printers and MFU',
                'sort' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        Categorygroup::insert($categorygroups);
    }

    public function categories()
    {
        $categories = [
            [
                'name' => 'Router',
                'categorygroup_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'PC',
                'categorygroup_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Laptop',
                'categorygroup_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Unmanagable Switch',
                'categorygroup_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Layer 2 Managable Switch',
                'categorygroup_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Layer 2 + Managable Switch',
                'categorygroup_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Layer 3 Managable Switch',
                'categorygroup_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Wi - Fi',
                'categorygroup_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Printer',
                'categorygroup_id' => 5,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Scaner',
                'categorygroup_id' => 5,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'MFU',
                'categorygroup_id' => 5,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        Category::insert($categories);
    }

    public function brands()
    {
        $brands = [
            [
                'name' => 'null',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Mikrotik',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Cisco',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'HP',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'IBM',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'D-Link',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Supermicro',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Huawei',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Ubiquiti',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Dell',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Kingston',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        Brand::insert($brands);
    }

    public function measurments()
    {
        $measurments = [
            [
                'name' => 'null',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'MHz',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Mbit/s',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'MB',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'pages/sec.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        Measurment::insert($measurments);
    }

    public function parametergroups()
    {
        $parametergroups = [
            [
                'name' => 'Main characteristics',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Other',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'CPU',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Video',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Display',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Configuration',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Drives',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Communications',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Printing parameters',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Interfaces, connectors and outputs',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Logistics (size, weight...)',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        Parametergroup::insert($parametergroups);
    }

    public function parameters()
    {
        $parameters = [
            [
                'name' => 'CPU name',
                'type' => 'text',
                'measurment_id' => 1,
                'parametergroup_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'CPU core count',
                'type' => 'number',
                'measurment_id' => 1,
                'parametergroup_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'CPU Frequency',
                'type' => 'number',
                'measurment_id' => 2,
                'parametergroup_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Bandwidth',
                'type' => 'number',
                'measurment_id' => 3,
                'parametergroup_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Printing Speed',
                'type' => 'number',
                'measurment_id' => 5,
                'parametergroup_id' => 9,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Wi-Fi Standards',
                'type' => 'text',
                'measurment_id' => 1,
                'parametergroup_id' => 8,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'RAM Size',
                'type' => 'number',
                'measurment_id' => 4,
                'parametergroup_id' => 6,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => '100 Mbit/s ports',
                'type' => 'number',
                'measurment_id' => 1,
                'parametergroup_id' => 10,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => '1000 Mbit/s ports',
                'type' => 'number',
                'measurment_id' => 1,
                'parametergroup_id' => 10,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        Parameter::insert($parameters);
    }

    public function products()
    {
        $products = [
            [
                'name' => 'Mikrotik hAP lite',
                'quantity' => random_int(1, 12),
                'price' => 27,
                'brand_id' => 3,
                'rating' => random_int(1, 10),
                'description' => 'The home Access Point lite (hAP lite) is an ideal little device for your apartment, house or office.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Cisco 1941',
                'quantity' => random_int(1, 12),
                'price' => 179,
                'brand_id' => 3,
                'rating' => random_int(1, 10),
                'description' => 'Cisco® 1941 builds on the best-in-class offering of the existing Cisco 1841 Integrated Services Routers by offering 2 models - Cisco 1941 and Cisco 1941W. In addition to the support of a wide range of wireless and wired connectivity options supported on Cisco 1941 Series, Cisco 1941W offers integration of IEEE 802.11n access point which is backwards compatible with IEEE 802.11a/b/g access points.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Cisco Catalyst 2970G-24T',
                'quantity' => random_int(1, 12),
                'price' => 2962,
                'brand_id' => 3,
                'rating' => random_int(1, 10),
                'description' => 'The Cisco® Catalyst® 2970 Series Switches are affordable Gigabit Ethernet switches that deliver wire-speed intelligent services for small and medium businesses and enterprise branch offices. Featuring a complete set of intelligent services, the Catalyst 2970 Series Switches can enhance network performance and increase productivity for today’s knowledge workers by intelligently scaling beyond 100 Mbps over existing Category 5 copper cabling.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'HP LaserJet Pro MFP M130a',
                'quantity' => random_int(1, 12),
                'price' => 174,
                'brand_id' => 4,
                'rating' => random_int(1, 10),
                'description' => 'Keep things simple with HP’s smallest LaserJet MFP – powered by JetIntelligence Toner cartridges. Print professional documents, plus scan, copy, and help save energy with a compact MFP designed for efficiency.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'D-Link DIR-632',
                'quantity' => random_int(1, 12),
                'price' => 23,
                'brand_id' => 6,
                'rating' => random_int(1, 10),
                'description' => 'USB Port
The router is equipped with a USB port for connecting a USB modem, which can be used to establish connection to the Internet. In addition, to the USB port of the router you can connect a USB storage device, which will be used as a network drive, or a printer.

Wireless Interface
Using the DIR-632 device, you are able to quickly create a wireless network at home or in your office, which lets your relatives or employees connect to your wireless network virtually anywhere (within the operational range of your wireless network). The router can operate as a base station for connecting wireless devices of the standards 802.11b, 802.11g, and 802.11n (at the rate up to 300Mbps).
The router supports multiple functions for the wireless interface: several security standards (WEP, WPA/WPA2), MAC address filtering, WPS, WMM.

8-port Switch
The built-in 8-port switch enables you to connect Ethernet-enabled computers, game consoles, and other devices to your network.

Security
The wireless router DIR-632 includes a built-in firewall. The advanced security functions minimize threats of hacker attacks, prevent unwanted intrusions to your network, and block access to unwanted websites for users of your LAN.
Built-in Yandex.DNS service protects against malicious and fraudulent web sites and helps to block access to adult content on children\'s devices.

Easy configuration and update
You can configure the settings of the wireless router DIR-632 via the user-friendly web-based interface (the interface is available in several languages).
The router itself checks the D-Link update server. If a new approved firmware is available, a notification will appear in the web-based interface of the router.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Dell Vostro 15 3568',
                'quantity' => random_int(1, 12),
                'price' => 420,
                'brand_id' => 10,
                'rating' => random_int(1, 10),
                'description' => 'Secure small business power.
15" (381mm) laptop ideal for professionals. Featuring a large screen, numeric keypad and essential security features.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        Product::insert($products);
    }

    public function images()
    {
        $images = [
            1 =>
                [
                    'name' => '55b5fa3d68c60915f2163f212c60c96a9256a9f61a95fedf3df21a3f149f1c09.jpg',
                    'product_id' => 1,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            2 =>
                [
                    'name' => 'Mikrotik hAP lite.jpg',
                    'product_id' => 1,
                    'main' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            3 =>
                [
                    'name' => '1076_hi_res.png',
                    'product_id' => 1,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            4 =>
                [
                    'name' => '1080_hi_res.png',
                    'product_id' => 1,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            5 =>
                [
                    'name' => 'mikrotik-rb941-top-ethernet-port-1024x680.jpg',
                    'product_id' => 1,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            6 =>
                [
                    'name' => '777567704_w0_h0_cisco1941_k9_1_nd.jpg',
                    'product_id' => 2,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            7 =>
                [
                    'name' => 'cisco-1941-k9-integrated-services-gigabit-router-2u-ip-base-security-package-[2]-39295-p.jpg',
                    'product_id' => 2,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            8 =>
                [
                    'name' => 's-l300.jpg',
                    'product_id' => 2,
                    'main' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            9 =>
                [
                    'name' => '274.p56fe5e7cbddcd.jpg',
                    'product_id' => 3,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            10 =>
                [
                    'name' => 'catalyst-2970.jpg',
                    'product_id' => 3,
                    'main' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            11 =>
                [
                    'name' => '2.jpg',
                    'product_id' => 4,
                    'main' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            12 =>
                [
                    'name' => '116278_2254_draft_large.jpg',
                    'product_id' => 5,
                    'main' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            13 =>
                [
                    'name' => '0163175_2.JPG',
                    'product_id' => 5,
                    'main' => false,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
        ];

        foreach ($images as $key => $image) {
            $images[$key]['src'] = 'images/products/' . $image['product_id'] . '/' . $image['name'];
            $images[$key]['size'] = filesize('images/products/'. $image['product_id'] . '/' . $image['name']);
        }

        Image::insert($images);
    }

    public function parameter_product()
    {

        $productsParameters = [
            1 => [
                1 => ['value' => 'QCA9533'],
                2 => ['value' => '1'],
                3 => ['value' => '650'],
                6 => ['value' => '802.11b/g/n'],
                7 => ['value' => '32'],
                8 => ['value' => '4'],
            ],
            2 => [
                7 => ['value' => '512'],
                9 => ['value' => '2'],
            ],
            3 => [
                9 => ['value' => '24'],
            ],
            4 => [
                3 => ['value' => '600'],
                5 => ['value' => '22'],
                7 => ['value' => '128'],
            ],
            5 => [
                1 => ['value' => 'Atheros AR7241/AR7242'],
                6 => ['value' => '802.11b/g/n'],
                7 => ['value' => '32'],
                8 => ['value' => '9'],
            ],
            6 => [
                1 => ['value' => 'Intel Core i5-7200U'],
                2 => ['value' => '2'],
                3 => ['value' => '7200'],
                6 => ['value' => '802.11b/g/n'],
                7 => ['value' => '8192'],
                9 => ['value' => '1']
            ],
        ];

        foreach ($productsParameters as $id => $productsParameter) {
            Product::find($id)->parameters()->sync($productsParameter);
        }
    }
}
