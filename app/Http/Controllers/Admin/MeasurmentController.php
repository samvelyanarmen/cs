<?php

namespace App\Http\Controllers\Admin;

use App\Measurment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class MeasurmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $measurments = Measurment::sortable()->paginate(10);
        return view('admin.measurment_index', compact('measurments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.measurment_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name.*' => 'required|max:255|unique:measurments,name'
        ]);

        $data = [];
        foreach ($request->name as $key => $value) {
            $data[] = [
                'name' => $request->name[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        Measurment::insert($data);

        return redirect()->route('measurment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $measurment = Measurment::find($id);
        return $measurment ? view('admin.measurment_show', compact('measurment')) : back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $measurment = Measurment::find($id);
        return $measurment ? view('admin.measurment_edit', compact('measurment')) : back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:measurments,id,' . $id
        ]);
        Measurment::find($id)->update(request(['name']));
        return redirect()->route('measurment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $measurment = Measurment::find($id);

        if (count($measurment->parameters) === 0) {
            $measurment->delete();
            return redirect()->route('measurment.index');
        }

        return back()->withErrors(['Parameters are using this measurment. Detach them first',
            $measurment->parameters()->pluck('name'),
        ]);
    }

    public function parametersInMeasurment($id)
    {
        return Measurment::find($id)->parameters()->pluck('name')->toJson();
    }
}
