<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Categorygroup;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('categorygroup:id,name')->sortable()->paginate(10);
        return view('admin.category_index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorygroups = Categorygroup::all()->sortBy('name')->pluck('name', 'id');

        return view('admin.category_create', compact('categorygroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name.*' => 'required|max:255|unique:categories,name',
            'uri.*' => 'required|unique:categorygroups,uri|regex:/([a-z0-9\-]+)/',
            'categorygroup_id.*' => 'nullable|required|exists:categorygroups,id',
        ]);

        $data = [];
        foreach ($request->name as $key => $value) {
            $data[] = [
                'name' => $request->name[$key],
                'uri' => $request->uri[$key],
                'categorygroup_id' => $request->categorygroup_id[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        Category::insert($data);

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        if ($category) {
            return view('admin.category_show', compact('category'));
        }
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $categorygroups = Categorygroup::all()->sortBy('name')->pluck('name', 'id');
        if ($category) {
            return view('admin.category_edit', compact('category', 'categorygroups'));
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,id,' . $id,
            'uri.*' => 'required|unique:categorygroups,uri|regex:/([a-z0-9\-]+)/',
            'categorygroup_id' => 'nullable|required|exists:categorygroups,id',
        ]);
        Category::find($id)->update(request(['name', 'uri', 'categorygroup_id']));

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::find($id)->delete();
        return redirect()->route('category.index');
    }

    public function productsInCategory($id)
    {
        return Category::find($id)->products()->pluck('name')->toJson();
    }
}
