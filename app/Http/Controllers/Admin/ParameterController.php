<?php

namespace App\Http\Controllers\Admin;

use App\Parameter;
use App\Parametergroup;
use App\Measurment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parameters = Parameter::with(['parametergroup:id,name', 'measurment:id,name'])->sortable()->paginate(10);
        return view('admin.parameter_index', compact('parameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parametergroups = Parametergroup::all()->sortBy('name');
        $measurments = Measurment::all()->sortBy('name');

        return view('admin.parameter_create', compact('parametergroups', 'measurments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name.*' => 'required|max:255|unique:parameters,name',
            'type.*' => 'required|not_in:0',
            'measurment_id.*' => 'nullable|required|exists:measurments,id',
            'parametergroup_id.*' => 'nullable|required|exists:parametergroups,id',
        ]);

        $data = [];
        foreach ($request->name as $key => $value) {
            $data[] = [
                'name' => $request->name[$key],
                'type' => $request->type[$key],
                'measurment_id' => $request->measurment_id[$key],
                'parametergroup_id' => $request->parametergroup_id[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        Parameter::insert($data);

        return redirect()->route('parameter.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parameter = Parameter::find($id);
        return $parameter ? view('admin.parameter_show', compact('parameter')) : back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parameter = Parameter::find($id);

        if ($parameter) {
            $parametergroups = Parametergroup::all()->sortBy('name');
            $measurments = Measurment::all()->sortBy('name');
            return view('admin.parameter_edit', compact('parameter', 'measurments', 'parametergroups'));
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:parameters,id,' . $id,
            'type' => 'required|not_in:0',
            'measurment_id' => 'nullable|required|exists:measurments,id',
            'parametergroup_id' => 'nullable|required|exists:parametergroups,id',
        ]);
        Parameter::find($id)->update(request(['name', 'type', 'measurment_id', 'parametergroup_id']));

        return redirect()->route('parameter.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Parameter::find($id)->delete();
        return redirect()->route('parameter.index');
    }

    public function productsInParameter($id)
    {
        return Parameter::find($id)->products()->pluck('name')->toJson();
    }
}
