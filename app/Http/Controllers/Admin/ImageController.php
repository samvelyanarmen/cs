<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\Product;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::with('product:id,name')->sortable()->paginate(10);
        return view('admin.image_index', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all()->sortBy('name')->pluck('name', 'id');

        return view('admin.image_create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'productfiles.*.*' => 'required|image',
            'product_id.*' => 'nullable|required|exists:products,id',
        ]);

        $data = [];
        if ($validator->passes()) {
            foreach ($request->productfiles as $key => $oneInputFiles) {
                foreach ($oneInputFiles as $oneInputFile) {
                    $fullFileName = $oneInputFile->getClientOriginalName();
                    $fileNameWithoutExtension = pathinfo($fullFileName, PATHINFO_FILENAME);
                    $fileExtension = $oneInputFile->getClientOriginalExtension();
                    $i = 1;
                    $duplicateFileName = $fullFileName;
                    $folderSrc = 'public/' . $request->product_id[$key] . '/';
                    while (Storage::disk('local')->exists($folderSrc . $duplicateFileName)):
                        $fileNameWithoutExtension = $fileNameWithoutExtension . ' (' . $i . ')';
                        $duplicateFileName = $fileNameWithoutExtension . '.' . $fileExtension;
                        $i++;
                    endwhile;
                    $fullFileName = $duplicateFileName;

                    $oneInputFile->storeAs($folderSrc, $fullFileName);

                    $data[] = [
                        'name' => $fileNameWithoutExtension,
                        'src' => 'storage/' . $request->product_id[$key] . '/' . $fullFileName,
                        'product_id' => $request->product_id[$key],
                        'main' => 0,
                        'size' => $oneInputFile->getClientSize(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                }
            }

            Image::insert($data);
            return json_encode(['result' => ['success']]);
        }

        return json_encode(['result' => $validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = Image::find($id);
        if ($image) {
            return view('admin.image_show', compact('image'));
        }

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image = Image::find($id);
        $products = Product::all()->sortBy('name')->pluck('name', 'id');
        if ($image) {
            return view('admin.image_edit', compact('image', 'products'));
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            //TODO: name-i regex-@ dzel
            'name' => 'required',
            'product_id' => 'nullable|required|exists:products,id',
        ]);
        $image = Image::find($id);
        if (!($image->name === $request->name && $image->product_id == $request->product_id)) {
            $fileExtensionWithDot = substr($image->src, strrpos($image->src, '.'));
            $newFileSrc = 'public/' . $request->product_id . '/' . $request->name . $fileExtensionWithDot;
            if (Storage::disk('local')->exists($newFileSrc)) {
                return back()->withErrors('File with same name already exists for selected product');
            }
            $oldFileSrc = str_replace_first('storage', 'public', $image->src);
            $image->update([
                'name' => $request->name,
                'product_id' => $request->product_id,
                'src' => 'storage/' . $request->product_id . '/' . $request->name . $fileExtensionWithDot
            ]);
            Storage::move($oldFileSrc, $newFileSrc);
        }

        return redirect()->route('image.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $urlInDb = Image::where('id', $id)->pluck('src')->toArray()[0];
        $fileUrl = str_replace('storage', 'public', $urlInDb);
        Storage::delete($fileUrl);
        Image::find($id)->delete();
        return redirect()->route('image.index');
    }

}