<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::sortable()->paginate(10);
        return view('admin.brand_index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name.*' => 'required|max:255|unique:brands,name',
            'uri.*' => 'required|unique:categorygroups,uri|regex:/([a-z0-9\-]+)/',
        ]);

        $data = [];
        foreach ($request->name as $key => $value) {
            $data[] = [
                'name' => $request->name[$key],
                'uri' => $request->uri[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        Brand::insert($data);

        return redirect()->route('brand.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($id);
        if ($brand) {
            return view('admin.brand_show', compact('brand'));
        }
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        if ($brand) {
            return view('admin.brand_edit', compact('brand'));
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:brands,id,' . $id,
            'uri.*' => 'required|unique:categorygroups,uri|regex:/([a-z0-9\-]+)/',
        ]);
        Brand::find($id)->update(request(['name', 'uri']));

        return redirect()->route('brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);

        if (count($brand->products) === 0) {
            $brand->delete();
            return redirect()->route('brand.index');
        }

        return back()->withErrors(['Products are attachet to this brand. Detach them first',
            $brand->products()->pluck('name'),
        ]);
    }

    public function productsInBrand($id)
    {
        return Brand::find($id)->products()->pluck('name')->toJson();
    }
}
