<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Category;
use App\Categorygroup;
use App\Parameter;
use App\Parametergroup;
use App\Product;
use App\Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Param;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function index()
    {
        $products = Product::with('brand')->sortable()->paginate(10);
        return view('admin.product_index', compact('products'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorygroups = Categorygroup::has('categories')->with('categories')->get()->sortBy('name');
        $parametergroups = Parametergroup::has('parameters')->with(['parameters', 'parameters.measurment'])->get()->sortBy('name');
        $brands = Brand::all()->sortBy('name');

        return view('admin.product_create', compact('categorygroups', 'parametergroups', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'quantity' => 'required|integer',
            'price' => 'required',
            'brand_id' => 'nullable|required|exists:brands,id',
            'productFiles.*' => 'required|image',
            'category_id.*' => 'nullable|required|exists:categories,id',
            'parameter_id.*' => 'nullable|required|exists:parameters,id',
            'value.*' => 'required|max:255',
        ]);
        if ($validator->passes()) {
            $parametersArray = [];

            if (count($request->parameter_id)) {
                foreach ($request->parameter_id as $key => $param_id) {
                    $parametersArray[$param_id] = ['value' => $request->value[$key]];
                }
            }

            $product = new Product;
            $product->name = $request->name;
            $product->quantity = $request->quantity;
            $product->price = $request->price;
            $product->brand_id = $request->brand_id;
            $product->rating = '0';
            $product->description = $request->description;
            $product->save();

            $categories = $request->category_id;

            $this->updateCategoriesParameters($categories, $parametersArray);
            $product->parameters()->sync($parametersArray);
            $product->categories()->sync($categories);

            $this->addImages($product->id, $request->productFiles);

            return json_encode(['result' => ['success']]);
        }

        return json_encode(['result' => $validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        $product = Product::find($id);
        $productCategories = $product->categories()->with('categorygroup')->get();

        $categorygroupCategories = [];
        if (count($productCategories)) {
            foreach ($productCategories as $productCategory) {
                $categorygroupCategories[$productCategory->categorygroup->name][] = $productCategory->name;
            }
        }

        $productParameters = $product->parameters()->with('parametergroup')->get();

        $parametergroupParameters = [];
        if (count($productParameters)) {
            foreach ($productParameters as $productParameter) {
                $parametergroupParameters[$productParameter->parametergroup->name][] = [
                    'name' => $productParameter->name,
                    'value' => $productParameter->pivot->value,
                ];
            }
        }

        return view('admin.product_show', compact('product', 'categorygroupCategories', 'parametergroupParameters'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        $product = Product::find($id);

        $categorygroups = Categorygroup::with('categories')->get()->sortBy('name');
        $parametergroups = Parametergroup::has('parameters')->with(['parameters', 'parameters.measurment'])->get()->sortBy('name');
        $brands = Brand::all()->sortBy('name')->pluck('name', 'id');

        if ($product) {
            return view('admin.product_edit', compact('product', 'categorygroups', 'parametergroups', 'brands'));
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'quantity' => 'required|integer',
            'price' => 'required',
            'brand_id' => 'nullable|required|exists:brands,id',
            'productFiles.*' => 'required|image',
            'category_id.*' => 'nullable|required|exists:categories,id',
            'parameter_id.*' => 'nullable|required|exists:parameters,id',
            'value.*' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            $product = Product::find($id);

//            Update category_parameter table
            $parametersArray = [];
            if (count($request->parameter_id)) {
                foreach ($request->parameter_id as $key => $param_id) {
                    $parametersArray[$param_id] = ['value' => $request->value[$key]];
                }
            }

            $this->detachCategoriesParameters($product->categories, $product->parameters, $product);
            if ($request->category_id && $request->parameter_id) {
                $this->updateCategoriesParameters($request->category_id, $parametersArray);
            }

//            Update table
            $product->update([
                'name' => $request->name,
                'quantity' => $request->quantity,
                'price' => $request->price,
                'brand_id' => $request->brand_id,
                'rating' => 0,
                'description' => $request->description,
            ]);

//        Delete images
            $deletedImagesIds = explode(',', $request->deletedImagesId);
            if ($deletedImagesIds[0] !== '') {
                foreach ($deletedImagesIds as $deletedImagesId) {
                    $urlInDb = Image::where('id', $deletedImagesId)->pluck('src')->toArray()[0];
                    $fileUrl = str_replace('storage', 'public', $urlInDb);
                    Storage::delete($fileUrl);
                    Image::find($deletedImagesId)->delete(); //TODO: stugel kareli e ardyoq mi harcumov (arrayov) jnjel db-ic
                }
            }

//        Set main image
            if ($request->mainImageId) {
                $product->images()->update(['main' => 0]);
                $product->images()->find($request->mainImageId)->update(['main' => 1]);
            }

//        Add new images
            if (count($request->productFiles)) {
                $this->addImages($product->id, $request->productFiles);
            }

//        Set new parameters and categories
            if (count($request->parameter_id)) {
                $product->parameters()->sync($parametersArray);
            } else {
                $product->parameters()->sync([]);
            }

            $product->categories()->sync($request->category_id);
            return json_encode(['result' => ['success']]);
        }

        return json_encode(['result' => $validator->errors()->all()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        $categories = $product->categories;
        $parameters = $product->parameters;
        $this->detachCategoriesParameters($categories, $parameters, $product);

        if (!count($product->carts) && !count($product->orders)) {
            Storage::deleteDirectory('public/' . $id);
            $product->images()->delete();
            $product->delete();
            return redirect()->route('product.index');
        }

        return back()->withErrors(['Unknown error']);
    }

    private function cartsOfProduct($id)
    {
        return Product::find($id)->carts()->pluck('id')->toJson();
    }

    private function ordersOfProduct($id)
    {
        return Product::find($id)->orders()->pluck('id')->toJson();
    }

    private function addImages($id, $productFiles)
    {
        $data = [];
        foreach ($productFiles as $productFile) {
            $fullFileName = $productFile->getClientOriginalName();
            $fileNameWithoutExtension = pathinfo($fullFileName, PATHINFO_FILENAME);
            $fileExtension = $productFile->getClientOriginalExtension();
            $i = 1;
            $duplicateFileName = $fullFileName;
            $folderSrc = 'public/' . $id . '/';
            while (Storage::disk('local')->exists($folderSrc . $duplicateFileName)):
                $duplicateFileName = $fileNameWithoutExtension . ' (' . $i . ').' . $fileExtension;
                $i++;
            endwhile;
            $fullFileName = $duplicateFileName;

            $productFile->storeAs($folderSrc, $fullFileName);

            $data[] = [
                'name' => $fileNameWithoutExtension,
                'src' => 'storage/' . $id . '/' . $fullFileName,
                'product_id' => $id,
                'main' => 0,
                'size' => $productFile->getClientSize(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        Image::insert($data);
    }

    private function updateCategoriesParameters($category_ids, $parametersArray)
    {
        if ($category_ids && $parametersArray) {
            $categories = Category::with('parameters')->find($category_ids);
            $parameters = Parameter::find(array_keys($parametersArray));
            foreach ($categories as $category) {
                $categoryData = [];
                $categoryParameters = $category->parameters->whereIn('id', array_keys($parametersArray));
                foreach ($parametersArray as $id => $parameter) {
                    $parameterFromDb = $parameters->where('id', '=', $id)->first();
                    $categoryParameter = $categoryParameters->where('id', '=', $id)->first();
                    if ($categoryParameter) {
                        $value = $parametersArray[$id]['value'];
                        $type = $categoryParameter->type;
                        $minvalue = $categoryParameter->pivot->minvalue;
                        $maxvalue = $categoryParameter->pivot->maxvalue;
                        $valueArr = json_decode($categoryParameter->pivot->valuearray);

                        if ($type === "number") {
                            if ($value < $minvalue) {
                                $categoryData[$id] = [
                                    'minvalue' => $value,
                                    'updated_at' => Carbon::now(),
                                ];
                            } elseif ($value > $maxvalue) {
                                $categoryData[$id] = [
                                    'maxvalue' => $value,
                                    'updated_at' => Carbon::now(),
                                ];
                            }
                        } else {
                            if (!in_array($value, $valueArr)) {
                                $valueArr[] = $value;
                                $categoryData[$id] = [
                                    'valuearray' => json_encode($valueArr),
                                    'updated_at' => Carbon::now(),
                                ];
                            }
                        }
                    } else {
                        $type = $parameterFromDb->type;
                        $value = $parameter['value'];
                        if ($type === "number") {
                            $categoryData[$id] = [
                                'minvalue' => $value,
                                'maxvalue' => $value,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                        } else {
                            $categoryData[$id] = [
                                'valuearray' => json_encode([$value]),
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                        }
                    }
                }
                $category->parameters()->syncWithoutDetaching($categoryData);
            }
        }
    }

    private function detachCategoriesParameters($categories, $parameters, $product)
    {
        foreach ($categories as $category) {
            foreach ($parameters as $parameter) {
                $type = $parameter->type;
                $value = $parameter->pivot->value;

                $categoryParameter = $category->parameters->where('id', $parameter->id)->first();

                if ($categoryParameter) {
                    $minvalue = $categoryParameter->pivot->minvalue;
                    $maxvalue = $categoryParameter->pivot->maxvalue;
                    $valueArr = json_decode($categoryParameter->pivot->valuearray);

                    if ($type === "number") {
                        if ($value == $minvalue) {
                            $categorysProductsWithCurrentParameter = $parameter->products()->whereHas('categories', function ($query) use ($category) {
                                $query->where('categories.id', $category->id);
                            })->get();
                            $productsParametersArray = [];
                            foreach ($categorysProductsWithCurrentParameter as $categorysProductWithCurrentParameter) {
                                $productsParametersArray[$categorysProductWithCurrentParameter->id] = $categorysProductWithCurrentParameter->pivot->value;
                            }
                            unset($productsParametersArray[$product->id]);
                            if ($productsParametersArray) {
                                $newMinValue = min($productsParametersArray);
                                $category->parameters()->syncWithoutDetaching([$parameter->id => ['minvalue' => $newMinValue]]);
                            } else {
                                $category->parameters()->detach($parameter->id);
                            }
                        } elseif ($value == $maxvalue) {
                            $categorysProductsWithCurrentParameter = $parameter->products()->whereHas('categories', function ($query) use ($category) {
                                $query->where('categories.id', $category->id);
                            })->get();
                            $productsParametersArray = [];
                            foreach ($categorysProductsWithCurrentParameter as $categorysProductWithCurrentParameter) {
                                $productsParametersArray[$categorysProductWithCurrentParameter->id] = $categorysProductWithCurrentParameter->pivot->value;
                            }
                            unset($productsParametersArray[$product->id]);
                            if ($productsParametersArray) {
                                $newMaxValue = max($productsParametersArray);
                                $category->parameters()->syncWithoutDetaching([$parameter->id => ['maxvalue' => $newMaxValue]]);
                            } else {
                                $category->parameters()->detach($parameter->id);
                            }
                        }
                    } else {
                        $categorysProductsWithCurrentParameter = $parameter->products()->whereHas('categories', function ($query) use ($category) {
                            $query->where('categories.id', $category->id);
                        })->get();
                        $productsParametersArray = [];
                        foreach ($categorysProductsWithCurrentParameter as $categorysProductWithCurrentParameter) {
                            $productsParametersArray[$categorysProductWithCurrentParameter->id] = $categorysProductWithCurrentParameter->pivot->value;
                        }
                        unset($productsParametersArray[$product->id]);
                        if ($productsParametersArray) {

//                        Ete ayd category-i voreve producti mot chka ayd value-n, apa jnjel ayd value-n arrayic
                            if (!in_array($value, $productsParametersArray)) {
                                unset($valueArr[array_search($value, $valueArr)]);
//                            Ete valueArr-@ datark che,apa sync anel category_parameter table-i hamapatasxan toxi het,
//                            hakarak depqum jnjel ayd tox@
                                if ($valueArr) {
                                    $category->parameters()->syncWithoutDetaching([$parameter->id => ['valuearray' => json_encode(array_values($valueArr))]]);
                                } else {
                                    $category->parameters()->detach($parameter->id);
                                }
                            }
                        } else {
                            $category->parameters()->detach($parameter->id);
                        }
                    }
                }
            }
        }
    }

}
