<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Parametergroup;

class ParametergroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parametergroups = Parametergroup::sortable()->paginate(10);
        return view('admin.parametergroup_index', compact('parametergroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parametergroup_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name.*' => 'required|max:255|unique:parametergroups,name'
        ]);

        $data = [];
        foreach ($request->name as $key => $value) {
            $data[] = [
                'name' => $request->name[$key],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        Parametergroup::insert($data);

        return redirect()->route('parametergroup.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parametergroup = Parametergroup::find($id);
        return $parametergroup ? view('admin.parametergroup_show', compact('parametergroup')) : back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parametergroup = Parametergroup::find($id);
        return $parametergroup ? view('admin.parametergroup_edit', compact('parametergroup')) : back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:parametergroups,id,' . $id
        ]);
        Parametergroup::find($id)->update(request(['name']));

        return redirect()->route('parametergroup.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parametergroup = Parametergroup::with('parameters')->find($id);

        if (!$parametergroup->parameters->count()) {
            $parametergroup->delete();
            return redirect()->route('parametergroup.index');
        }

        return back()->withErrors([
            'Parametergroup is not empty. Delete parameters first',
            $parametergroup->parameters()->pluck('name'),
        ]);
    }

    public function parametersInParametergroup($id)
    {
        return Parametergroup::find($id)->parameters()->pluck('name')->toJson();
    }
}
