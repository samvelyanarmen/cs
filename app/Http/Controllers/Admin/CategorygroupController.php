<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categorygroup;

class CategorygroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorygroups = Categorygroup::all()->sortBy('sort');
        return view('admin.categorygroup_index', compact('categorygroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categorygroup_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name.*' => 'required|max:255|unique:categorygroups,name',
            'uri.*' => 'required|unique:categorygroups,uri|regex:/([a-z0-9\-]+)/',
        ]);

        $maxSort = Categorygroup::max('sort');
        $data = [];
        foreach ($request->name as $key => $value) {
            $data[] = [
                'name' => $request->name[$key],
                'uri' => $request->uri[$key],
                'sort' => ++$maxSort,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        Categorygroup::insert($data);

        return redirect()->route('categorygroup.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categorygroup = Categorygroup::find($id);

        return $categorygroup ? view('admin.categorygroup_show', compact('categorygroup')) : back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorygroup = Categorygroup::find($id);
        if ($categorygroup) {
            return view('admin.categorygroup_edit', compact('categorygroup'));
        }
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:categorygroups,id,' . $id,
            'uri.*' => 'required|unique:categorygroups,uri|regex:/([a-z0-9\-]+)/',
        ]);
        Categorygroup::find($id)->update(request(['name', 'uri']));

        return redirect()->route('categorygroup.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorygroup = Categorygroup::with('categories')->find($id);

        if (!$categorygroup->categories->count()) {
            $categorygroup->delete();
            return redirect()->route('categorygroup.index');
        }

        return back()->withErrors([
            'Categorygroup is not empty. Delete categories first',
            $categorygroup->categories()->pluck('name'),
        ]);
    }

    public function updateSortOrder(Request $request)
    {
        $sortValues = $request->sortValues;
        foreach ($sortValues as $id => $sort) {
            Categorygroup::find($id)->update(['sort' => $sort]);
        }
        return redirect()->route('categorygroup.index');
    }

    public function categoriesInCategorygroup($id)
    {
        return Categorygroup::find($id)->categories()->pluck('name')->toJson();
    }

}
