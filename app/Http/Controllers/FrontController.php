<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Categorygroup;
use App\Product;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $brands = Brand::has('products')->with('products')->get()->sortBy('name');
        return view('front.index', compact('brands'));
    }
}
