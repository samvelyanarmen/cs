<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Categorygroup;
use Illuminate\Http\Request;

class CategorygroupController extends Controller
{
    public function index($categorygroupUri)
    {
        $selectedCategorygroup = Categorygroup::where('uri', $categorygroupUri)->first();

//        TODO: If Categorygroup does not exists, send to 404 page

        $productsId = $selectedCategorygroup->productsInCategorygroup()->pluck('id')->toArray();
        $brands = Brand::brandsWithProducts($productsId);

        $priceMin = $selectedCategorygroup->productsInCategorygroup()->min('price');
        $priceMax = $selectedCategorygroup->productsInCategorygroup()->max('price');


        return view('front.categorygroup', compact('selectedCategorygroup', 'brands', 'priceMin', 'priceMax'));
    }


}
