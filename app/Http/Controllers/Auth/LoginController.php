<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {

        if ($user->hasVerifiedEmail()) {
            echo json_encode(['result' => 'logged_in']);
        } else {
            $this->guard()->logout();
            echo json_encode([
                'result' => 'verify_email',
                'resend_link' => route('auth.verify.resend') . '?email=' . $user->email
            ]);
        }
    }

    protected function login(Request $request)
    {

        if (Auth::guest()) {

            $credentials = $request->only('email', 'password');
            $authSuccess = Auth::attempt($credentials, $request->keep_signed_in);

            if ($authSuccess) {
                $user = Auth::user();
                $this->authenticated($request, $user);
            } else {
                echo json_encode(['result' => 'wrong_credentials']);
            }

        }
    }
}