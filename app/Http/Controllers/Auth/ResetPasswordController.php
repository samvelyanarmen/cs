<?php

namespace App\Http\Controllers\Auth;

use Mail;
use Hash;
use App\User;
use App\Mail\SendResetEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendPasswordResetEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->passes()) {

            $user = User::where('email', $request->email)->first();
            if ($user) {
                $sendMessage = new SendResetEmail($user);
                Mail::to($user->email)->send($sendMessage);
                return response()->json(['result' => ['success']]);
            }
            return response()->json(['result' => ['user does not exist']]);

        }
        return response()->json(['result' => $validator->errors()->all()]);
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->passes()) {
            $user = User::where('email', $request->email)->where('remember_token', $request->token)->first();
            if ($user !== null) {
                $password = Hash::make($request->password);

                $user->update(['password' => $password]);
                return response()->json(['result' => ['success']]);
            }
        }
        return response()->json(['result' => ['error']]);
        $this->guard()->logout();
    }
}
