<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\VerificationToken;
use Auth;
use App\Events\UserRequestedVerificationEmail;

class VerificationController extends Controller
{
    /**
     * @param VerificationToken $token
     * @return mixed
     * @throws \Exception
     */
    public function verify(VerificationToken $token)
    {
        // Получаем соответствующего пользователя и обговляем 'verified' на true
        // а потом удаляем token и перенаправляем пользователя на маршрут(  /login )
        $token->user()->update([
            'verified' => true
        ]);
        $token->delete();

        return redirect('/#login')->withInfo('Your email successfully confirmed. Please login again.');
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function resend(Request $request)
    {
        $user = User::byEmail($request->email)->firstOrFail();
        if ($user->hasVerifiedEmail()) {
            echo json_encode([
                'result' => 'already_verified',
            ]);
        } else {
            event(new UserRequestedVerificationEmail($user));
            echo json_encode([
                'result' => 'email_sent',
            ]);
        }
    }
}