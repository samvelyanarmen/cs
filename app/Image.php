<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Image extends Model
{
    use Sortable;

    protected $fillable = ['name', 'src', 'product_id', 'main', 'size'];
    public $sortable = ['id', 'name', 'product', 'main', 'created_at', 'updated_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
