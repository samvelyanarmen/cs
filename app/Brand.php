<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use App\Product;

class Brand extends Model
{
    use Sortable;

    protected $fillable = ['name', 'uri'];
    public $sortable = ['id', 'name',  'uri', 'created_at', 'updated_at'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public static function brandsWithProducts(array $productsId)
    {
        return Brand::whereHas('products', function ($query) use ($productsId) {
            $query->whereIn('products.id', $productsId);
        })->with('products')->get()->sortBy('name');
}
}
