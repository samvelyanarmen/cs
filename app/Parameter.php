<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Parameter extends Model
{
    use Sortable;
    protected $fillable = ['name', 'type', 'measurment_id', 'parametergroup_id'];
    public $sortable = ['id', 'name', 'type', 'measurment', 'parametergroup', 'created_at', 'updated_at'];

    public function parametergroup()
    {
        return $this->belongsTo(Parametergroup::class);
    }

    public function measurment()
    {
        return $this->belongsTo(Measurment::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('value');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withPivot('minvalue', 'maxvalue','valuearray');
    }
}
