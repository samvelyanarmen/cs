<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorygroup extends Model
{
    protected $fillable = ['name', 'uri', 'sort'];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function productsInCategorygroup()
    {
        $categoryIds = $this->categories->pluck('id');
        $products = Product::whereHas('categories', function ($query) use ($categoryIds) {
            $query->whereIn('categories.id', $categoryIds);
        });
        return $products;
    }
}
