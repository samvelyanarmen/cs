<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Category extends Model
{
    use Sortable;

    protected $fillable = ['name', 'uri', 'categorygroup_id'];
    public $sortable = ['id', 'name', 'uri', 'created_at', 'updated_at'];

    public function categorygroup()
    {
        return $this->belongsTo(Categorygroup::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function parameters()
    {
        return $this->belongsToMany(Parameter::class)->withPivot('minvalue', 'maxvalue','valuearray');
    }
}
