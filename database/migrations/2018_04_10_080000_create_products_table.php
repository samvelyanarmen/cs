<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('quantity');
            $table->float('price');
            $table->integer('brand_id')->unsigned();
            $table->integer('rating');
            $table->longText('description')->nullable();
            $table->timestamps();

//            TODO: avelacnel meta dashter
//            $table->string('meta_d')->nullable();
//            $table->string('meta_k')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
