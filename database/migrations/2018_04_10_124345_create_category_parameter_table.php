<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_parameter', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('parameter_id')->unsigned();
            $table->primary(['category_id','parameter_id']);
            $table->integer('minvalue')->nullable();
            $table->integer('maxvalue')->nullable();
            $table->string('valuearray')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_parameter');
    }
}
